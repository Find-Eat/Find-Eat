import sys
import os
import pandas as pd
import numpy
import botometer
import re
import random
from fuzzywuzzy import fuzz

#Lee el archivo de tweets
nombre_archivo = sys.argv[1]
tweets = pd.read_excel(nombre_archivo, engine='openpyxl')

#Archivo de restaurantes
restaurantes_maps = pd.read_csv('restaurantes_maps.csv', sep=';')

#Limpiar los datos
cln_restaurantes_maps = restaurantes_maps
cln_tweets = tweets
cln_restaurantes_maps['name'] = cln_restaurantes_maps['name'].str.lower()
cln_tweets['entities.annotations'] = cln_tweets['entities.annotations'].str.lower()
cln_tweets['text'] = cln_tweets['text'].str.lower()
cln_restaurantes_maps['name'] = cln_restaurantes_maps['name'].str.replace('restaurante', '')
cln_tweets['entities.annotations'] = cln_tweets['entities.annotations'].str.replace('restaurante', '')
cln_tweets['text'] = cln_tweets['text'].str.replace('restaurante', '')

restaurantes_info = pd.read_csv('restaurantes_maps.csv', sep=';')
tweets_info = pd.read_excel(nombre_archivo, engine='openpyxl')

def retornaSimilitudTexto(restaurante, cuerpo):
	return fuzz.token_set_ratio(restaurante, cuerpo)
	
def retornaSimilitudEntities(restaurante, string):
    place_sim = pd.DataFrame(columns={'similitud'}) 
    
    for l in string:
        if l == '{':
            #Substring de lo que está dentro de {}
            in_idx_subs = string.find("{") + 1 
            fin_idx_subs = string.find("}") + 3
            subs = string[in_idx_subs:fin_idx_subs]

            #Halla el valor normalized_text (nombre del lugar)
            pattern = "'type': 'place', 'normalized_text': '"

            #Si encuentra una anotación sobre lugar en el string
            if (re.search(pattern, subs) == None) is False:
                match=(re.search(pattern, subs))
                indice_inicial = match.end()
                indice_final = subs.find("'", indice_inicial)
                norm = subs[indice_inicial:indice_final]

                #Halla la similitud entre la etiqueta de lugar y el restaurante en estudio
                similitud = fuzz.token_set_ratio(restaurante, norm)
                place_sim = place_sim.append({'similitud':similitud}, ignore_index=True)

                #Retira el substring {} del string inicial
                string = string[fin_idx_subs:len(string)]
            #Si es cualquier otra etiqueta diferente de la de lugar, la retira
            else:
                #Retira el substring {} del string inicial
                string = string[fin_idx_subs:len(string)] 

    #Si no encontró etiquetas de lugar devuelve 0
    if place_sim.empty:
        return 0
    else:
        #Devuelve la etiqueta que tuvo mayor similitud
        return place_sim['similitud'].max()

#Halla similitud de nombre de restaurante restaurante en texto o en entities.annotations de tweet con FuzzyWuzzy
def coincidencia_tweets_rest():

    restaurantes = cln_restaurantes_maps
    tweets_rest = pd.DataFrame(columns={"text", "entities.annotations", "maps_place_id", "maps_business_status", "maps_east", "maps_lat", "maps_lng", "maps_name",
                                    "maps_north", "maps_price_level", "maps_rating", "maps_south", "maps_user_ratings_total", "maps_vicinity",
                                    "maps_west"})

    #Por cada tweet...
    for x in range(len(cln_tweets)):
        temporal = pd.DataFrame(columns={"text", "entities.annotations", "maps_place_id",	"maps_business_status", "maps_east",	"maps_lat",	"maps_lng",	"maps_name",
                                            "maps_north",	"maps_price_level",	"maps_rating",	"maps_south",	"maps_user_ratings_total",	"maps_vicinity",
                                            "maps_west", "similitud"})

        entitie = cln_tweets.iloc[x]['entities.annotations']
        cuerpo = cln_tweets.iloc[x]['text']

        #...evalúa la similitud con cada nombre de restaurante                                     
        for y in range(len(restaurantes)):
            nombre_restaurante = restaurantes.iloc[y]['name']

			#Si no hay datos en entities.annotations busca la coincidencia en text
            if pd.isna(entitie) or entitie == '':
                similitud = retornaSimilitudTexto(nombre_restaurante, cuerpo)
            else:
                similitud = retornaSimilitudEntities(nombre_restaurante, entitie)
            
            if similitud == None:
                similitud = 0

            #Solamente si la similitud es mayor a 75, agrega al dataframe 
            if similitud >= 70:     
                #Guarda todos los que tuvieron similitud > 75 en un DataFrame temporal              
                temporal = temporal.append({'text':tweets_info.iloc[x]['text'], 'entities.annotations':tweets_info.iloc[x]['entities.annotations'], 
                                        'maps_name':restaurantes_info.iloc[y]['name'], 
                                        'maps_place_id':restaurantes.iloc[y]['place_id'],
                                        'maps_business_status':restaurantes.iloc[y]['business_status'],
                                        'maps_east':restaurantes.iloc[y]['east'],
                                        'maps_lat':restaurantes.iloc[y]['lat'],'maps_north':restaurantes.iloc[y]['north'],
                                        'maps_price_level':restaurantes.iloc[y]['price_level'],
                                        'maps_rating':restaurantes.iloc[y]['rating'],
                                        'maps_south':restaurantes.iloc[y]['south'],
                                        'maps_user_ratings_total':restaurantes.iloc[y]['user_ratings_total'],
                                        'maps_vicinity':restaurantes.iloc[y]['vicinity'],
                                        'maps_west':restaurantes.iloc[y]['west'], 'similitud':similitud},
                                        ignore_index=True)

        #Al terminar de recorrer todo el DataFrame de restaurantes, si encontró alguna coincidencia entra 
        if temporal.empty is False:    
            #Solamente guarda en el DataFrame resultante el restaurante con mayor similitud que esté en el dt temporal 
            #Si hay más de uno que tiene el mismo valor que el de mayor similitud, escoge solo uno al azar
            tweets_rest = tweets_rest.append((temporal[temporal['similitud']==temporal['similitud'].max()]).sample())
    return tweets_rest

#Chequea el puntaje de BotOMeter de los autores de los tweets
def verificaAutores(autores):

    #Credenciales de acceso a BotOMeter
    rapidapi_key = "key"
    twitter_app_auth = {
        'consumer_key':'key',
        'consumer_secret':'key',
        'access_token':'key', 
        'access_token_secret':'key',
    }

    #Conexión con BotOMeter
    bom = botometer.Botometer(wait_on_ratelimit=True,
                            rapidapi_key=rapidapi_key,
                            **twitter_app_auth)

    #Creación de DataFrame author id - bot result
    df_es_bot = pd.DataFrame(columns=['author_id', 'bot_result'])

    #Check account in - columna de author id de los tweets 
    #Puntaje de 0 a 1: 0 es humano 1 es bot
    for screen, res in bom.check_accounts_in(autores):

        #Si la cuenta permite ser verificada, guarda el resultado
        try:
            resultado = res['cap']
            df_es_bot = df_es_bot.append({'author_id':screen, 'bot_result':resultado['universal']}, ignore_index=True)

        #Si no permite verificar, se guarda como puntaje indeterminado (0.5) 
        except Exception:
            df_es_bot = df_es_bot.append({'author_id':screen, 'bot_result':0.5}, ignore_index=True)
            print(Exception)
    return df_es_bot

#Union de la información de los tweets con los tweets que tuvieron coicidencia con nombres de restaurantes 
try:
    tr = coincidencia_tweets_rest()
    tweets_df = pd.merge(tweets_info, tr, on='text')   
except Exception:  
    print("No se encontró ninguna coindencia de restaurantes en los tweets :)")


#DataFrame final, union de DataFrame de resultados de bot, con el DataFrame de coincidencia tweets-restaurantes
tweets_bot_df = pd.merge(verificaAutores(tweets_df['author_id']), tweets_df, on="author_id") 
tweets_bot_df.to_excel('tweets_' + nombre_archivo + '.xlsx')

