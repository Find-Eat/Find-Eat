#Bloque import

import tweepy
import pandas as pd
import json
import datetime
import requests
import botometer
import re
import boto3

#URL de full-archive del Twitter API

search_url = "https://api.twitter.com/2/tweets/search/all"

#Definición del query

since_date = '2016-01-01T00:00:00.00Z'
until_date = '2020-01-01T00:00:00.00Z'
max = 500

#Las coordenadas indicadas en point_radius:[-74.0956466 4.6584796 12mi] son las coordenadas del centro estimado de Bogotá D.C
#Se definen los campos que se tendrán en la respuesta del API (tweet, lugar, usuario y expansiones)

query_params = {'query':"(restaurante OR RESTAURANTE) point_radius:[-74.0956466 4.6584796 12mi] lang:es has:geo",
                'start_time': since_date,
                'end_time': until_date,
                'max_results': max,
                'tweet.fields':'author_id,context_annotations,created_at,entities,geo,id,lang,possibly_sensitive',
                'place.fields':'country,country_code,full_name,id,place_type,geo',
                'user.fields':'name,location,public_metrics,verified',
                'expansions':'author_id,entities.mentions.username,geo.place_id'
                }

#Llave secreta para el acceso al API desde la cuenta académica

bearer_token = 'token'

#

def create_headers(bearer_token):
    headers = {"Authorization": "Bearer {}".format(bearer_token)}
    return headers

def connect_to_endpoint(url, headers, params):
    response = requests.request("GET", search_url, headers=headers, params=params)
    #print(response.status_code)
    if response.status_code != 200:
        raise Exception(response.status_code, response.text)
    return response.json()

def main():
    headers = create_headers(bearer_token)
    json_response = connect_to_endpoint(search_url, headers, query_params)
    return json_response

respuesta = main()
resp = pd.json_normalize(respuesta)
user_df = pd.DataFrame()
tweets_df = pd.json_normalize(respuesta, "data")

for x in resp['includes.users']:
    user_df = pd.json_normalize(x)

user_df = user_df.rename(columns = {'id':'author_id', 'location':'user_profile_location'})
tweets_df = pd.merge(tweets_df, user_df, on='author_id')

#Credenciales de acceso
rapidapi_key = "key"
twitter_app_auth = {
    'consumer_key':'key',
    'consumer_secret':'key',
    'access_token':'token', 
    'access_token_secret':'token',
}

#Acceso
bom = botometer.Botometer(wait_on_ratelimit=True,
                        rapidapi_key=rapidapi_key,
                        **twitter_app_auth)

#Creación de DataFrame
df_es_bot = pd.DataFrame(columns=['author_id', 'bot_result'])

#Por cada id de autor que está en el DataFrame tweets_df, chequea si puede ser un bot y guarda el resultado en un nuevo DataFrame df_es_bot
for screen, res in bom.check_accounts_in(tweets_df.author_id):
    resultado = res['cap']
    df_es_bot = df_es_bot.append({'author_id':screen, 'bot_result':resultado['universal']}, ignore_index=True)

tweets_df = pd.merge(tweets_df, df_es_bot, on='author_id')

array = []
for x in range(0, len(tweets_df)):  
    tweets_array = {}
    tweets_array['id'] = str(tweets_df.iloc[x]['id'])
    tweets_array['text'] = str(tweets_df.iloc[x]['text']) 
    tweets_array['verified'] = str(tweets_df.iloc[x]['verified'])
    tweets_array['followers'] = str(tweets_df.iloc[x]['public_metrics.followers_count'])
    tweets_array['bot_result'] = str(tweets_df.iloc[x]['bot_result'])
    array.append(tweets_array)

print(len(array))

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('Tweets')
for i in range(0, len(array)):
   table.put_item(Item=array[i])

print('ya :)')
