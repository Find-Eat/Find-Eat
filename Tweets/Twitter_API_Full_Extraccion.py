#Bloque import

import tweepy
import pandas as pd
import numpy as np
import json
from datetime import datetime as dt
import time as tm
from time import sleep
import datetime
import requests
import botometer
import sys
import boto3
import matplotlib.pyplot as plt
plt.style.use(['dark_background'])
if sys.version_info[0] >= 3:
    from datetime import timezone
"""
#URL de full-archive del Twitter API

search_url = "https://api.twitter.com/2/tweets/search/all"

#Llave secreta para el acceso al API desde la cuenta académica

bearer_token = 'key'

#Método para esperar a Twitter API
#Evita la Exception: Request returned an error: 429 {"title":"Too Many Requests","type":"about:blank","status":429,"detail":"Too Many Requests"}

def pause_until(time): 

    end = time

    # If we're on Python 3 and the user specified a timezone,
    # convert to UTC and get tje timestamp.
    if sys.version_info[0] >= 3 and time.tzinfo:
        end = time.astimezone(timezone.utc).timestamp()
    else:
        zoneDiff = tm.time() - (dt.now() - dt(1970, 1, 1)).total_seconds()
        end = (time - dt(1970, 1, 1)).total_seconds() + zoneDiff

    # Now we wait
    while True:
        now = tm.time()
        diff = end - now

        #
        # Time is up!
        #
        if diff <= 0:
            break
        else:
            # 'logarithmic' sleeping to minimize loop iterations
            sleep(diff / 2)

#Conexión al API

def create_headers(bearer_token):
    headers = {"Authorization": "Bearer {}".format(bearer_token)}
    return headers

def connect_to_endpoint(url, headers, params):

    response = requests.request("GET", search_url, headers=headers, params=params)
    
    # Twitter returns (in the header of the request object) how many
    # requests you have left. Lets use this to our advantage

    remaining_requests = int(response.headers["x-rate-limit-remaining"])
    
    # If that number is one, we get the reset-time
    #   and wait until then, plus 15 seconds (your welcome Twitter).
    # The regular 429 exception is caught below as well,
    #   however, we want to program defensively, where possible.

    if remaining_requests == 1:
        buffer_wait_time = 15
        resume_time = dt.fromtimestamp( int(response.headers["x-rate-limit-reset"]) + buffer_wait_time )
        print(f"Waiting on Twitter.\n\tResume Time: {resume_time}")
        pause_until(resume_time)  ## Link to this code in above answer

    # We still may get some weird errors from Twitter.
    # We only care about the time dependent errors (i.e. errors
    #   that Twitter wants us to wait for).
    # Most of these errors can be solved simply by waiting
    #   a little while and pinging Twitter again - so that's what we do.

    if response.status_code != 200:

        # Too many requests error

        if response.status_code == 429:
            buffer_wait_time = 15
            resume_time = dt.fromtimestamp( int(response.headers["x-rate-limit-reset"]) + buffer_wait_time )
            print(f"Waiting on Twitter.\n\tResume Time: {resume_time}")
            pause_until(resume_time)  ## Link to this code in above answer

        # Twitter internal server error

        elif response.status_code == 500:

            # Twitter needs a break, so we wait 30 seconds

            resume_time = datetime.now().timestamp() + 30
            print(f"Waiting on Twitter.\n\tResume Time: {resume_time}")
            pause_until(resume_time)  ## Link to this code in above answer

        # Twitter service unavailable error

        elif response.status_code == 503:

            # Twitter needs a break, so we wait 30 seconds

            resume_time = datetime.now().timestamp() + 30
            print(f"Waiting on Twitter.\n\tResume Time: {resume_time}")
            pause_until(resume_time)  ## Link to this code in above answer

        # If we get this far, we've done something wrong and should exit

        raise Exception(
            "Request returned an error: {} {}".format(
                response.status_code, response.text
            )
        )

    # Each time we get a 200 response, lets exit the function and return the response.json
    if response.ok:
        return response.json()
    else: print("Valió")

def main():
    headers = create_headers(bearer_token)
    json_response = connect_to_endpoint(search_url, headers, query_params)
    return json_response

#DataFrame para guardar la cantidad de resultados por lote de días

resultados = pd.DataFrame(columns=["Anual", "Mensual", "Semanal", "Tiempo_año", "Tiempo_mes", "Tiempo_semana"])

	
#Definición de variables para query de búsqueda

since_date = '2016-01-01T00:00:00.00Z'
until_date = since_date
top_date = '2021-01-01T00:00:00.00Z'
max = 500

#DataFrame para guardar los resultados por lote de días

tweets_df_anio = pd.DataFrame()
tweets_df_mes = pd.DataFrame()
tweets_df_semana = pd.DataFrame()

#Extraer datos por lotes

#MES

#Toma de tiempo de la extracción de datos

start_time = tm.time()

#Iteraciones por mes

while until_date < top_date:  
	
	#Suma la cantidad de días a la fecha de inicio para asignarla a la fecha -hasta- de la consulta

	since_datetime = datetime.datetime.strptime(since_date,"%Y-%m-%dT%H:%M:%S.%fZ")
	until_date = (since_datetime + datetime.timedelta(days=30)).strftime("%Y-%m-%dT%H:%M:%S.%fZ")

	#Las coordenadas indicadas en point_radius:[-74.0956466 4.6584796 12mi] son las coordenadas del centro estimado de Bogotá D.C
	#Se definen los campos que se tendrán en la respuesta del API (tweet, lugar, usuario y expansiones)

	query_params = {'query':"(restaurante OR RESTAURANTE) point_radius:[-74.0956466 4.6584796 12mi] lang:es has:geo",
				'start_time': since_date,
				'end_time': until_date,
				'max_results': max,
				'tweet.fields':'author_id,created_at,entities,geo,id,lang,possibly_sensitive',
				'place.fields':'country,country_code,full_name,id,place_type,geo',
				'user.fields':'name,location,public_metrics,verified',
				'expansions':'author_id,entities.mentions.username,geo.place_id'
				}

	#Se recibe la respuesta del API

	respuesta = main()

	#Se convierte el JSON a DataFrame para lograr trabajar con la información con mayor facilidad

	resp = pd.json_normalize(respuesta)
	user_df = pd.DataFrame()
	tweets_temp = pd.json_normalize(respuesta, "data")

	for x in resp['includes.users']:
		user_df = pd.json_normalize(x)

	user_df = user_df.rename(columns = {'id':'author_id', 'location':'user_profile_location'})
	tweets_temp = pd.merge(tweets_temp, user_df, on='author_id')

	#Agrega el DataFrame temporal de la respuesta JSON, al DataFrame final de año

	tweets_df_mes = tweets_df_mes.append(tweets_temp)
	
	#Suma la cantidad de días a la fecha de inicio - MES

	since_datetime = datetime.datetime.strptime(since_date,"%Y-%m-%dT%H:%M:%S.%fZ")
	since_date = (since_datetime + datetime.timedelta(days=30)).strftime("%Y-%m-%dT%H:%M:%S.%fZ")
	
	#Full-archive also has a 1 request / 1 second limit
	sleep(1)
	
	print("Mes" + str(since_date))

#Toma de tiempo final

tiempo_mes = tm.time() - start_time

#Guardar resultados en Excel

tweets_df_mes.to_excel(r'resultados_prueba_it_mensual_' + str(i) + '.xlsx', sheet_name="Mensual", index = False)

#conexión con la base de datos

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('Tweets')
for i in range(0, len(array)):
   table.put_item(Item=array[i])
