import tkinter as tk
from tkinter.filedialog import askopenfilename
import json
import boto3

dynamo_list = []
dynamodb = boto3.resource('dynamodb')

def getFile():
    """[A function that gets a json file]

    Returns:
        [String]: [Return the json file name]
    """
    root = tk.Tk()
    root.withdraw() 
    filename = askopenfilename() 
    return filename


def createArray(fileName):
    """[A function that creates an array with data from json file]

    Args:
        fileName ([String]): [Gets a json file name]

    Returns:
        [Array]: [Return an array with the data]
    """
    f = open(fileName,encoding="utf8")
    data = json.load(f)
    for i in data:
        main_list = {}
        main_list['name'] = i['name']
        main_list['business_status'] = i['business_status']
        main_list['lat'] = str(i['lat'])
        main_list['lng'] = str(i['lng'])
        main_list['south'] = str(i['south'])
        main_list['west'] = str(i['west'])
        main_list['north'] = str(i['north'])
        main_list['east'] = str(i['east'])
        main_list['place_id'] = str(i['place_id'])
        if 'rating' in i:
            main_list['rating'] = str(i['rating'])
        if 'price_level' in i:
            main_list['price_level'] = str(i['price_level'])
        if 'user_ratings_total' in i:
            main_list['user_ratings_total'] = str(i['user_ratings_total'])
        main_list['vicinity'] = str(i['vicinity'])
        dynamo_list.append(main_list)
    f.close()
    return dynamo_list

def upData(dynamo_list):
    """[A function to up data to dynamodb]

    Args:
        dynamo_list ([Array]): [Get the data to upload to dynamodb]
    """
    table = dynamodb.Table('Restaurants')
    for i in range(0, len(dynamo_list)):
        table.put_item(Item=dynamo_list[i])
    

def readData():
    """[A function to read data from dynamodb]
    """
    table_scan = dynamodb.Table('Restaurants')
    response = table_scan.scan()
    data_scan = response['Items']
    print(data_scan)

if __name__ == "__main__":
    file = getFile()
    data = []
    data = createArray(file)
    upData(data)
    readData()
