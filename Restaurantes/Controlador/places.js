var map;

function key(){
  fetch('../Controlador/data.json')
   .then(response => {
       if (!response.ok) {
           throw new Error("HTTP error " + response.status);
       }
       return response.json();
   })
   .then(json => {
       this.key = json.key;
       console.log("Ok");
       document.getElementById('script_1').src = "https://maps.googleapis.com/maps/api/js?key="+this.key+"&libraries=places&callback=createMap";  
   })
   .catch(function () {
       this.dataError = true;
   })
}

function createMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {
      lat: 4.6581844,
      lng: -74.0930954
    },
    zoom: 10
  });



  var request = {
    location: map.getCenter(),
    radius: 6000,
    types: ['restaurant']
  }

  var service = new google.maps.places.PlacesService(map);

  service.nearbySearch(request, callback);
}
const results = [];

const callback = (response, status, pagination) => {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
      results.push(...response);
  }

  if (pagination.hasNextPage) {
      setTimeout(() => pagination.nextPage(), 2000);
  } else {
    downloadObjectAsJson(results, 'Restaurants'+new Date());
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
    }
  }
}

/* function callback(results, status,pagination) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    console.log(results.length);
  }
  if (pagination.hasNextPage) {
    pagination.nextPage();
  } else {
    console.log(results.length);
    downloadObjectAsJson(results, 'Prueba');
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
    }
  } 
  
} */

function downloadObjectAsJson(exportObj, exportName){
    const prueba = [];
    for (var i = 0; i < exportObj.length; i++) {
      prueba.push({name:exportObj[i].name, business_status:exportObj[i].business_status, lat:exportObj[i].geometry.location.lat(),
      lng:exportObj[i].geometry.location.lng(), south:exportObj[i].geometry.viewport.getSouthWest().lat(), west: exportObj[i].geometry.viewport.getSouthWest().lng(),
      north: exportObj[i].geometry.viewport.getNorthEast().lat(), east: exportObj[i].geometry.viewport.getNorthEast().lng(), place_id: exportObj[i].place_id, 
      rating: exportObj[i].rating, price_level: exportObj[i].price_level, user_ratings_total: exportObj[i].user_ratings_total,
      vicinity: exportObj[i].vicinity});
    }
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(prueba));
    var downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute("href",     dataStr);
    downloadAnchorNode.setAttribute("download", exportName + ".json");
    document.body.appendChild(downloadAnchorNode); // required for firefox
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
  }

function createMarker(place) {
  var placeLoc = place.geometry.location;
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location,
    title: place.name

  })
  const infowindow = new google.maps.InfoWindow({
    content: place.name + "<br>" + place.rating + "<br>" + place.geometry.location + "<br>" + place.price_level,
    maxWidth: 200,
  });
  marker.addListener("click", () => {
    infowindow.open(map, marker);
  });

}

