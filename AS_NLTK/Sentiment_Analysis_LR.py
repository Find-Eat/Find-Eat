"""Descarga e instalacion de librerias"""
import warnings
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.tokenize import word_tokenize
from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report,confusion_matrix
import tweepy
import numpy as np
import matplotlib.pyplot as plt
import re
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
nltk.download('stopwords')
stemmer = SnowballStemmer('spanish')
nltk.download('punkt')
warnings.filterwarnings("ignore")
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.model_selection import learning_curve
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_predict
import sklearn.metrics as metrics
import boto3
import boto3
from collections import Counter

"""Entrenamiento del modelo"""

"""Lee la data de un Google Sheet: experimentos.analitica.datos - EncuestaCineColombiano_Respuestas"""
df = pd.read_csv("https://docs.google.com/spreadsheets/d/e/2PACX-1vQC3CXrmRk6mpK9-DrpO--faGVT_KsR8rj-AToUfFlsbKNnUB2wVslmNYiFT1pv80Z5gp76tgSqp1aN/pub?gid=1802142849&single=true&output=tsv", sep="\t")
df.columns = ['A','B','C','D','E']
"""Transforma el dataset en Opinion-Tipo """
good_df = df[['C']]
good_df['Opinion'] = "POSITIVE" 
bad_df = df[['D']]
bad_df.columns = ['C']
bad_df['Opinion'] = "NEGATIVE" 
df_op = pd.concat([good_df,bad_df])
df_op.columns = ['Opinion','Type']
print(df_op)
df_op.groupby(['Type']).count()

"""Eliminacion de conectores"""
stop_words = set(stopwords.words('spanish')) 
stop_words = stop_words.union(set(['me', 'le', 'da', 'mi', 'su', 'ha', 'he', 'ya', 'un', 'una', 'es','del', 'las', 'los', 'en', 'que', 'y', 'la','de']))

def fast_preproc(text):
  """[summary]
  Funcion utilizada para remover conectores
  Args:
      text ([type]): Recibe el texto normal

  Returns:
      [type]: Devuelve el texto sin conectores
  """
  words = word_tokenize(text.lower())
  words = [word for word in words if not word in stop_words] 
  words = [stemmer.stem(word) for word in words]  
  try:
    text = " ".join(str(word) for word in words)
  except Exception as e:
    print(e)
    pass
  return text

df_op['Opinion'] = df_op['Opinion'].astype(str)
df_op = df_op.assign(
    TextPreproc=lambda df: df_op.Opinion.apply(fast_preproc)
)
df_op.head()

"""Divide el dataset"""
X = df_op['TextPreproc']
Y = df_op['Type']

vec = TfidfVectorizer()
"""Construccion del vocabulario"""
vec.fit(X)
print("\nVocabulary:\n")
print(vec.vocabulary_)
"""Codificacion del documento"""
trans_text_train = vec.transform(X)
df = pd.DataFrame(trans_text_train.toarray(), columns=vec.get_feature_names())
df.head()

"""Definicion de muestras de entrenamiento y testeo"""

X_train, X_test, Y_train, Y_test = train_test_split(df, Y, test_size=0.15)
print("Train and test shapes:\n")
print("Train. X: " + str(X_train.shape) + " - Y:" + str(Y_train.shape))
print("Test. X: " + str(X_test.shape) + " - Y:" + str(Y_test.shape))

"""PARAMETER TUNNING"""

pipeLR = Pipeline([('classifier', LogisticRegression())])
pipeDT = Pipeline([('classifier', tree.DecisionTreeClassifier())])
pipeRF = Pipeline([('classifier', RandomForestClassifier())])


param_gridLR = [
              {'classifier' : [LogisticRegression()],
               'classifier__penalty' : ['l2'],
               'classifier__C' : np.logspace(-4, 4, 20)}
]
param_gridDT =  [
              {'classifier' : [tree.DecisionTreeClassifier()],
               'classifier__max_depth' : list(range(2,12,2)),
               'classifier__criterion' : ['gini', 'entropy'],
               'classifier__max_features' : list(range(1,2,1))}
]
param_gridRF = [
              {'classifier' : [RandomForestClassifier()],
               'classifier__n_estimators' : list(range(10,101,10)),
               'classifier__max_depth' : list(range(2,12,2)),
               'classifier__max_features' : list(range(1,2,1))}
]

clfLR = GridSearchCV(pipeLR, param_grid = param_gridLR, cv = 10, verbose=True, n_jobs=-1, scoring='accuracy', return_train_score=True)
clfDT = GridSearchCV(pipeDT, param_grid = param_gridDT, cv = 10, verbose=True, n_jobs=-1, scoring='accuracy', return_train_score=True)
clfRF = GridSearchCV(pipeRF, param_grid = param_gridRF, cv = 10, verbose=True, n_jobs=-1, scoring='accuracy', return_train_score=True)

print("\nTunning Regresion Logistica:\n")
best_clfLR = clfLR.fit(X_train,Y_train)
print("\nTunning Arbol de decision:\n")
best_clfDT = clfDT.fit(X_train,Y_train)
print("\nTunning Random Forest:\n")
best_clfRF = clfRF.fit(X_train,Y_train)



print("\n------MEJORES PARAMETROS------\n")

print("\nRegresion Logistica:\n")
print(best_clfLR.best_params_)
print("\nArboles de Decision:\n")
print(best_clfDT.best_params_)
print("\nRandom Forest:\n")
print(best_clfRF.best_params_)

"""Resultados Comparacion Mean Train Scroe Vs Mean Test Score"""
"""Regresion Logistica"""
results= pd.DataFrame(best_clfLR.cv_results_);
print("\n------RRESULTADOS REGRESION LOGISTICA------\n")
results[['mean_train_score','mean_test_score']]
train_scores = best_clfLR.cv_results_['mean_train_score'] 
test_scores = best_clfLR.cv_results_['mean_test_score']
plt.plot(test_scores, label='testLR')
plt.plot(train_scores, label='trainLR')
plt.legend(loc='best')
plt.show()

"""Arboles de Decision"""
resultsDT= pd.DataFrame(best_clfDT.cv_results_);
print("\n------RRESULTADOS ARBOLES DE DECISION------\n")
resultsDT[['mean_train_score','mean_test_score']]
train_scores = best_clfDT.cv_results_['mean_train_score'] 
test_scores = best_clfDT.cv_results_['mean_test_score']
plt.plot(test_scores, label='testDT')
plt.plot(train_scores, label='trainDT')
plt.legend(loc='best')
plt.show()

"""Random Forest"""
resultsRF= pd.DataFrame(best_clfRF.cv_results_);
print("\n------RRESULTADOS RANDOM FOREST------\n")
resultsRF[['mean_train_score','mean_test_score']]
train_scores = best_clfRF.cv_results_['mean_train_score'] 
test_scores = best_clfRF.cv_results_['mean_test_score']
plt.plot(test_scores, label='testRF')
plt.plot(train_scores, label='trainRF')
plt.legend(loc='best')
plt.show()




"""MATRIZ DE CONFUSION DE LOS MODELOS"""
y_predLR = best_clfLR.best_estimator_.predict(X_test)
metrics.accuracy_score(Y_test, y_predLR)
y_predDT = best_clfDT.best_estimator_.predict(X_test)
metrics.accuracy_score(Y_test, y_predDT)
y_predRF =best_clfRF.best_estimator_.predict(X_test)
metrics.accuracy_score(Y_test, y_predRF)
print("\nRegresion Logistica\n")
print("\nMatriz de confusión:\n")
print(confusion_matrix(Y_test,y_predLR))
print("\nEstadisticas del clasificador:\n")
print(classification_report(Y_test,y_predLR))
print("\nDecision Trees\n")
print("\nMatriz de confusión:\n")
print(confusion_matrix(Y_test,y_predDT))
print("\nEstadisticas del clasificador:\n")
print(classification_report(Y_test,y_predDT))
print("\nRandom Forest\n")
print("\nMatriz de confusión:\n")
print(confusion_matrix(Y_test,y_predRF))
print("\nEstadisticas del clasificador:\n")
print(classification_report(Y_test,y_predRF))

"""LECTURA DE TWEETS"""
classifier = best_clfLR.best_estimator_
new_opinion = ["Me parece normal el restaurante de el Corral"]
Xt_new = [fast_preproc(str(new_opinion))]
trans_new_doc = vec.transform(Xt_new) #Use same TfIdfVectorizer
print("\nPredicted result: " + str(classifier.predict(trans_new_doc)))


"""LECUTRA DE TWEETS EXTRAIDOS DE DYNAMODB"""
#clean text
#function to clean tweets
def cleanTxt(text):
  text = re.sub(r'@[A-Za-z0-9]+', '', text) #Removed @mentions
  text = re.sub(r'#', '', text) #removed hashtag
  text = re.sub(r'RT[\s]+', '', text) #Removing RT
  text = re.sub(r'https?:\/\/\S+', '', text) #removed links
  return text
#Clean text


dynamodb = boto3.resource('dynamodb')
table_scan = dynamodb.Table('Tweets')
response = table_scan.scan()
data_scan = response['Items']
df = pd.DataFrame(data_scan)
with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    print(df)
df1_op =df[['bot_result','text','maps_place_id']]

with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    print(df1_op)

lista=[]
for index, row in df1_op.iterrows():
  Xt_new = [fast_preproc(str(row['text']))]
  trans_new_doc = vec.transform(Xt_new)
  lista.append(str(classifier.predict(trans_new_doc)))
se = pd.Series(lista)
df1_op['polarity'] = se.values

with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    print(df1_op)

contador1= Counter(df1_op['polarity'])
P = contador1["['POSITIVE']"]
N = contador1["['NEGATIVE']"]
print("Positivos "+str(P))
print("Negativos "+str(N))


listy= []
for x in range(len(df1_op)):
    aux = df1_op.iloc[x]['maps_place_id']
    listy.append(aux)
    print(aux)
print(listy)

cleanedList = [x for x in listy if str(x) != 'nan']

print(cleanedList)

puntaje = 0.0

for x in range(len(cleanedList)):
    botorNot = 0.0
    contBot = 0
    calificaciones = []
    aux = cleanedList[x]
    for y in range(len(df1_op)):
        if(aux == df1_op.iloc[y]['maps_place_id']):
            calificaciones.append(df1_op.iloc[y]['polarity'])
            botorNot= botorNot + float(df1_op.iloc[y]['bot_result'])
            contBot = contBot +1
            
    contador=Counter(calificaciones)
    P = contador["['POSITIVE']"]
    N = contador["['NEGATIVE']"]
    print("Positivos"+str(P))
    print("Negativos"+str(N))
    botorNot= botorNot/contBot
    TOTAL= P+N
    if(TOTAL>0):  
        porcentaje= (P*100)/TOTAL
        puntaje=(porcentaje*5)/100
        print("Puntaje bot "+str(botorNot))

        if(botorNot>=0.5 and puntaje>0.0):
            puntaje= puntaje*0.60 - ((5*botorNot)*0.25)+5*0.15
            print("Puntaje total tras aplicar formula cercana a bot "+str(puntaje))
            output = [aux, puntaje]
            print(output[0] , output[1])
            dynamo = []
            lista ={}
            lista['porcentaje'] = str(porcentaje)
            lista['place_id'] = aux
            lista['rating'] = str(puntaje)
            dynamo.append(lista)
            print(dynamo)  
            dynamodb = boto3.resource('dynamodb')
            table = dynamodb.Table('SentimentA')
            for i in range(0, len(dynamo)):
              table.put_item(Item=dynamo[i])
        elif(botorNot<0.5 and puntaje>0.0):
            puntaje= puntaje*0.80 - ((5*botorNot)*0.05)+5*0.15
            print("Puntaje total tras aplicar formula lejana a bot "+str(puntaje))
            output = [aux, puntaje]
            print(output[0] , output[1])
            dynamo = []
            lista ={}
            lista['porcentaje'] = str(porcentaje)
            lista['place_id'] = aux
            lista['rating'] = str(puntaje)
            dynamo.append(lista)
            print(dynamo)       
            dynamodb = boto3.resource('dynamodb')
            table = dynamodb.Table('SentimentA')
            for i in range(0, len(dynamo)):
              table.put_item(Item=dynamo[i])            





