// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:findandeat/providers/rest_provider.dart';
import 'package:findandeat/providers/user_provider.dart';
import 'package:findandeat/src/models/list_favorites_model.dart';
import 'package:findandeat/src/models/user_model.dart';
import 'package:findandeat/src/pages/login_page.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('UserServices', () {
    test('User should start at null', () {
      expect(getUsuario.user, null);
    });
    test('User should be obtained', () {
      final userProv = UserProvider();
      Future<bool> resp =
          userProv.validarUsuario('12344', validation, usuarioLogin);
      userProv.validarUsuario('12344', validation, usuarioLogin).then((value) =>
          {expect(getUsuario.user, 'ppp'), expect(getUsuario.state, 1)});
    });
    test('User should be created', () {
      final userProv = UserProvider();
      int codResp;
      UserModel user = new UserModel();
      user.user = 'UserPruebaUnitaria';
      user.name = 'ValuePruebaUnitaria';
      user.pass = '1234567890';
      user.state = 1;
      userProv
          .createUser(user, codResp)
          .then((value) => {expect(codResp, 200)});
    });
    test('User Created should be visible', () {
      final userProv = UserProvider();
      userProv.validarUsuario('1234567890', validation, usuarioLogin).then(
          (value) => {
                expect(getUsuario.user, 'UserPruebaUnitaria'),
                expect(getUsuario.state, 1)
              });
    });
  });
  group('RestaurantService', () {
    /***test('List of restaurants should be obtained', () {
      final restProv = RestaurantProvider();
      restProv
          .cargarRestaurantes(listRest, mejores10)
          .then((value) => {expect(listRest.length, 1515)});
    });*/
    test('List of suggestions should be obtained', () {
      final restProv = RestaurantProvider();
      restProv
          .cargarNombreRestaurantes(suggestions)
          .then((value) => {expect(suggestions.length, 92)});
    });
  });
  group('FavoriteService', () {
    test('Restaurant should be added to User Favorite List', () {
      final favProv = UserProvider();
      FavoriteModel fav = new FavoriteModel();
      fav.user = 'pp';
      fav.restaurant = 'restUnitario';
      int status = 0;
      favProv
          .addFavoriteRestaurant(fav, status)
          .then((value) => {expect(status, 200)});
    });
    test('Favorite List should be obtained per user', () {
      final favProv = UserProvider();
      UserModel user = new UserModel();
      user.user = 'pp';
      user.name = 'value3';
      user.pass = '12344';
      user.state = 1;
      favProv
          .cargarRestaurantesFavoritosUsuario(getListFav, user)
          .then((value) => {expect(getListFav[0].restaurant, 'prueba2')});
    });
    test('Restaurant should be deleted from User Favorite List', () {
      final favProv = UserProvider();
      FavoriteModel fav = new FavoriteModel();
      fav.user = 'pp';
      fav.restaurant = 'restUnitario';
      int status = 0;
      favProv
          .deleteFavoriteRestaurant(fav, status)
          .then((value) => {expect(status, 200)});
    });
  });
}
