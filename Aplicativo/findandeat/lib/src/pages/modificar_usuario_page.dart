import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:findandeat/providers/user_provider.dart';
import 'package:findandeat/src/models/user_model.dart';
import 'package:findandeat/src/pages/home_page.dart';
import 'package:findandeat/src/pages/login_validation.dart';
import 'package:flutter/material.dart';
import 'package:findandeat/src/bloc/provider.dart';
import 'package:findandeat/src/pages/login_page.dart';
import 'package:http/http.dart' as http;

//Variables Globales
int statusCode;
set setCode(int value) {
  statusCode = value;
}

//Clase principal de tipo statefull
class ModificarInfoScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return Modificar();
  }
}

//Clase que cambia la informacion de la clase principal
class Modificar extends State<ModificarInfoScreen> {
  bool puede = false;
  String _name;
  String _email;
  TextEditingController _passwordActual = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _rPassword = TextEditingController();
  UserModel usuarioARegistar = new UserModel();
  UserProvider user;
  bool isApiCallProcessRegister = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return LoginVal(
      child: _uiRegister(context),
      inAsyncCall: isApiCallProcessRegister,
      opacity: 0.3,
    );
  }

  @override
  //Widget que acopla la informacion
  Widget _uiRegister(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        _crearFondo(context),
        _registerForm(context),
      ],
    ));
  }

  //Widget que muestra los campos para registrarse
  Widget _registerForm(BuildContext context) {
    final bloc = Provider.of(context);
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
        child: Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 180.0,
            ),
          ),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 30.0),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 3.0,
                      offset: Offset(0.0, 5.0),
                      spreadRadius: 3.0)
                ]),
            child: Column(
              children: <Widget>[
                Text('Cambia tu Información', style: TextStyle(fontSize: 20.0)),
                SizedBox(height: 15.0),
                _eliminarCuenta(bloc),
                SizedBox(height: 15.0),
                _crearNombre(),
                SizedBox(height: 30.0),
                _crearPassword(),
                SizedBox(height: 30.0),
                _crearNuevaPassword(),
                SizedBox(height: 30.0),
                _repetirNuevaPassword(),
                SizedBox(height: 30.0),
                _confirmarRegistro(bloc),
                SizedBox(height: 30.0),
                _crearBotonCancelar(bloc),
              ],
            ),
          ),
          SizedBox(height: 100.0)
        ],
      ),
    ));
  }

  //Widget que crea el campo del nombre
  Widget _crearNombre() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: TextFormField(
          decoration: InputDecoration(
            icon: Icon(Icons.person, color: Colors.blueGrey),
            hintText: 'Nombre y Apellido',
            labelText: usuarioLogin.name,
          ),
          maxLength: 20,
          validator: (String value) {
            if (value.isNotEmpty) {
              Pattern pattern2 = r'^[a-zA-Z ñáéíóúÁÉÍÓÚ]+$';
              if (!RegExp(pattern2).hasMatch(value)) {
                return 'El nombre solo puede tener letras';
              }
            }
            return null;
          },
          onSaved: (String value) {
            if (value.isEmpty) {
              value = usuarioLogin.name;
            }
            _name = value;
            usuarioLogin.name = value;
          },
        ));
  }

  //Widget que crea el campo del Password
  Widget _crearPassword() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: TextFormField(
          controller: _passwordActual,
          obscureText: true,
          decoration: InputDecoration(
            icon: Icon(Icons.lock_outline, color: Colors.blueGrey),
            hintText: 'Ejemplo88!',
            labelText: '*****',
          ),
          maxLength: 20,
          validator: (String value) {
            if (value.isNotEmpty) {
              var bytes5 = utf8.encode(value);
              var digest5 = sha256.convert(bytes5);
              if (digest5.toString() == usuarioLogin.pass) {
                puede = true;
              }

              if (value.length < 8) {
                return 'Mínimo 8 caracteres';
              }

              Pattern pattern3 =
                  r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]{8,}$';
              if (!RegExp(pattern3).hasMatch(value)) {
                return 'Mínimo 1 numero, 1 Mayúscula, 1 Minúscula, y un caracter especial';
              }
            }

            return null;
          },
          onSaved: (String value) {
            _passwordActual.text = value;
          },
        ));
  }

  Widget _crearNuevaPassword() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: TextFormField(
          controller: _password,
          obscureText: true,
          decoration: InputDecoration(
              icon: Icon(Icons.lock_outline, color: Colors.blueGrey),
              hintText: 'Ingrese su nueva Contraseña',
              labelText: 'Nueva Contraseña'),
          maxLength: 20,
          validator: (String value) {
            if (value.isNotEmpty) {
              if (puede == true) {
                if (value.length < 8) {
                  return 'Mínimo 8 caracteres';
                }
                Pattern pattern3 =
                    r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]{8,}$';
                if (!RegExp(pattern3).hasMatch(value)) {
                  return 'Mínimo 1 numero, 1 Mayúscula, 1 Minúscula, y un caracter especial';
                }
              } else {
                return 'Revise su clave Actual';
              }
            }

            return null;
          },
          onSaved: (String value) {
            if (value.isNotEmpty) {
              _password.text = value;
              var bytes2 = utf8.encode(value);
              var digest2 = sha256.convert(bytes2);
              usuarioLogin.pass = digest2.toString();
              usuarioLogin.state = 1;
            }
          },
        ));
  }

  //Widget que crea el campo del Password para repetirlo
  Widget _repetirNuevaPassword() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: TextFormField(
          controller: _rPassword,
          obscureText: true,
          decoration: InputDecoration(
              icon: Icon(Icons.lock_outline, color: Colors.blueGrey),
              hintText: 'Ingrese su Contraseña',
              labelText: 'Repita su nueva Contraseña'),
          maxLength: 20,
          validator: (String value) {
            if ((_password.text != _rPassword.text) && value.isNotEmpty) {
              return 'Las contraseñas deben coincidir';
            }

            return null;
          },
        ));
  }

  //Widget que confirma el registro
  Widget _confirmarRegistro(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Actualizar Datos'),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 0.0,
            color: Color.fromRGBO(0, 73, 141, 1.0),
            textColor: Colors.white,
            onPressed: () {
              if (validateAndSave()) {
                setState(() {
                  isApiCallProcessRegister = true;
                });
                int statusModify;
                UserProvider userProv = new UserProvider();
                userProv
                    .modifyUser(usuarioLogin, statusModify)
                    .then((value) => {
                          setState(() {
                            isApiCallProcessRegister = false;
                          }),
                        });

                createAlertDialogValido(context);
              } else {
                //createAlertDialogInvalido(context);
              }
            });
      },
    );
  }

  //Funcion que envia al email los datos del usuario
  Future sendEmail(String name, String email, String password) async {
    final serviceId = 'service_0luc2ug';
    final templateId = 'template_gehua14';
    final userId = 'user_JvFtxpTKFfJ54xAKi8QHe';
    final url = Uri.parse('https://api.emailjs.com/api/v1.0/email/send');
    final response = await http.post(url,
        headers: {
          'origin': 'http://localhost',
          'Content-Type': 'application/json'
        },
        body: json.encode({
          'service_id': serviceId,
          'template_id': templateId,
          'user_id': userId,
          'template_params': {
            'username': email,
            'name': name,
            'user_subject': 'Find&Eat Actualización de datos exitosa',
            'password': password
          }
        }));
  }

  //Widget que confirma el registro
  Widget _eliminarCuenta(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Eliminar Cuenta'),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 0.0,
            color: Color.fromRGBO(0, 73, 141, 1.0),
            textColor: Colors.white,
            onPressed: () {
              createAlertDialogEliminacion(context);
            });
      },
    );
  }

  //Funcion Future que valida la creacion del usuario
  Future<String> createAlertDialogEliminacion(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text(
                '¿Esta Seguro que desea Eliminar su cuenta de Find&Eat? \nSe perdera toda la información de la cuenta'),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Eliminar Cuenta'),
                  onPressed: () {
                    createAlertDialogConfirmacionEliminacion(context);
                  }),
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Cancelar'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomePage()));
                  }),
            ],
          );
        });
  }

  //Funcion Future que valida la creacion del usuario
  Future<String> createAlertDialogConfirmacionEliminacion(
      BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text(
              '¿Estas segur@?',
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Sí'),
                  onPressed: () {
                    setState(() {
                      isApiCallProcessRegister = true;
                    });
                    int statusCode;
                    UserProvider userProv = new UserProvider();
                    usuarioLogin.state = 0;
                    userProv.modifyUser(usuarioLogin, statusCode).then(
                          (value) => setState(() {
                            isApiCallProcessRegister = false;
                          }),
                        );
                    createAlertDialogMensajeEliminacion(context);
                  }),
              MaterialButton(
                  elevation: 5.0,
                  child: Text('No'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomePage()));
                  }),
            ],
          );
        });
  }

  //Funcion Future que valida la creacion del usuario
  Future<String> createAlertDialogMensajeEliminacion(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text(
              '¿Cuenta Eliminada con exitó?',
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Salír'),
                  onPressed: () {
                    usuarioLogin = new UserModel();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  }),
            ],
          );
        });
  }

  //Funcion Future que valida la creacion del usuario
  Future<String> createAlertDialogValido(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text('Usuario Modificado con Exito'),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Cerrar Mensaje'),
                  onPressed: () {
                    sendEmail(
                        usuarioLogin.name, usuarioLogin.user, _password.text);
                    puede = false;
                    _name = '';
                    _passwordActual = new TextEditingController();
                    _password = new TextEditingController();
                    _rPassword = new TextEditingController();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomePage()));
                  }),
            ],
          );
        });
  }

  //Funcion Future que invalida la creacion del usuario
  Future<String> createAlertDialogInvalido(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text('No se pudo modificar el usuario'),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Cerrar Mensaje'),
                  onPressed: () {
                    puede = false;
                    _name = '';
                    _passwordActual = new TextEditingController();
                    _password = new TextEditingController();
                    _rPassword = new TextEditingController();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomePage()));
                  }),
            ],
          );
        });
  }

  //Funcion que valida los campos
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  //Widget que crea el boton y cancela la operacion de registro
  Widget _crearBotonCancelar(LoginBloc bloc) {
    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Cancelar'),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 0.0,
            color: Color.fromRGBO(0, 73, 141, 1.0),
            textColor: Colors.white,
            onPressed: () {
              puede = false;
              _name = '';
              _passwordActual = new TextEditingController();
              _password = new TextEditingController();
              _rPassword = new TextEditingController();
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => HomePage()));
            });
      },
    );
  }

  //Widget que crea el fondo
  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final fondodegradado = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Color.fromRGBO(0, 73, 141, 1.0),
        Color.fromRGBO(90, 70, 178, 1.0)
      ])),
    );

    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          color: Color.fromRGBO(255, 255, 255, 0.05)),
    );

    return Stack(
      children: <Widget>[
        fondodegradado,
        Positioned(top: 90.0, left: 30.0, child: circulo),
        Positioned(top: -40.0, right: -30.0, child: circulo),
        Positioned(bottom: -50.0, right: -10.0, child: circulo),
        Positioned(bottom: 120.0, right: 20.0, child: circulo),
        Positioned(bottom: -50.0, left: -20.0, child: circulo),
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Icon(Icons.food_bank, color: Colors.white, size: 100.0),
              SizedBox(height: 10.0, width: double.infinity),
              Text('FIND & EAT',
                  style: TextStyle(color: Colors.white, fontSize: 25.0))
            ],
          ),
        )
      ],
    );
  }
}
