import 'package:findandeat/src/pages/restaurant_page.dart';
import 'package:flutter/material.dart';
import 'package:findandeat/src/bloc/provider.dart';
import 'package:findandeat/src/models/restaurant_model.dart';

//Clase Principal de tipo Stateless
class CommentsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    return Scaffold(
      appBar: AppBar(
          title: Text('Comentarios'),
          actions: <Widget>[],
          backgroundColor: Color.fromRGBO(0, 73, 141, 1.0)),
      body: Stack(children: <Widget>[
        _crearFondo(context),
        _commentForm(context),
      ]),
    );
  }
}

//Widget que crea el fondo
Widget _crearFondo(BuildContext context) {
  final size = MediaQuery.of(context).size;

  final fondodegradado = Container(
    height: size.height * 0.4,
    width: double.infinity,
    decoration: BoxDecoration(
        gradient: LinearGradient(colors: <Color>[
      Color.fromRGBO(0, 73, 141, 1.0),
      Color.fromRGBO(90, 70, 178, 1.0)
    ])),
  );

  final circulo = Container(
    width: 100.0,
    height: 100.0,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(255, 255, 255, 0.05)),
  );

  return Stack(
    children: <Widget>[
      fondodegradado,
      Positioned(top: 90.0, left: 30.0, child: circulo),
      Positioned(top: -40.0, right: -30.0, child: circulo),
      Positioned(bottom: -50.0, right: -10.0, child: circulo),
      Positioned(bottom: 120.0, right: 20.0, child: circulo),
      Positioned(bottom: -50.0, left: -20.0, child: circulo),
      Container(
        padding: EdgeInsets.only(top: 30.0),
        child: Column(
          children: <Widget>[
            Icon(Icons.food_bank, color: Colors.white, size: 100.0),
            SizedBox(height: 10.0, width: double.infinity),
            Text('FIND & EAT',
                style: TextStyle(color: Colors.white, fontSize: 25.0))
          ],
        ),
      )
    ],
  );
}

//Widget que plasma la informacion del restaurante favorito
Widget _commentForm(BuildContext context) {
  final size = MediaQuery.of(context).size;
  return Scaffold(
    body: DefaultTabController(
      length: 1,
      child: Scaffold(
        body: TabBarView(
          children: <Widget>[
            ListViewBuilder(),
          ],
        ),
      ),
    ),
  );
}

//Clase secundaria que crea la lista de restaurantes favoritos
class ListViewBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(12.0),
            child: Container(
              height: 50,
              color: Colors.blueGrey,
              child: Center(
                  child: RichText(
                      text: TextSpan(children: <TextSpan>[
                TextSpan(text: getListTweets[index])
              ]))),
            ),
          );
        },
        itemCount: getListTweets.length);
  }
}
