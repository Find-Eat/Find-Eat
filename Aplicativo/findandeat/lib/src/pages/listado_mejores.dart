import 'package:findandeat/src/pages/login_page.dart';
import 'package:findandeat/src/pages/restaurant_page.dart';
import 'package:flutter/material.dart';
import 'package:findandeat/src/bloc/provider.dart';
import 'package:findandeat/src/models/restaurant_model.dart';

//Variables Globales
RestaurantModel rest;
RestaurantModel get getRest => rest;

//Clase Principal de tipo Stateless
class BestRestaurantsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    return Scaffold(
      appBar: AppBar(
          title: Text('Mejores Restaurantes'),
          actions: <Widget>[],
          backgroundColor: Color.fromRGBO(0, 73, 141, 1.0)),
      body: Stack(children: <Widget>[
        _crearFondo(context),
        _bestForm(context),
      ]),
    );
  }
}

//Widget que crea el fondo
Widget _crearFondo(BuildContext context) {
  final size = MediaQuery.of(context).size;

  final fondodegradado = Container(
    height: size.height * 0.4,
    width: double.infinity,
    decoration: BoxDecoration(
        gradient: LinearGradient(colors: <Color>[
      Color.fromRGBO(0, 73, 141, 1.0),
      Color.fromRGBO(90, 70, 178, 1.0)
    ])),
  );

  final circulo = Container(
    width: 100.0,
    height: 100.0,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(255, 255, 255, 0.05)),
  );

  return Stack(
    children: <Widget>[
      fondodegradado,
      Positioned(top: 90.0, left: 30.0, child: circulo),
      Positioned(top: -40.0, right: -30.0, child: circulo),
      Positioned(bottom: -50.0, right: -10.0, child: circulo),
      Positioned(bottom: 120.0, right: 20.0, child: circulo),
      Positioned(bottom: -50.0, left: -20.0, child: circulo),
      Container(
        padding: EdgeInsets.only(top: 30.0),
        child: Column(
          children: <Widget>[
            Icon(Icons.food_bank, color: Colors.white, size: 100.0),
            SizedBox(height: 10.0, width: double.infinity),
            Text('FIND & EAT',
                style: TextStyle(color: Colors.white, fontSize: 25.0))
          ],
        ),
      )
    ],
  );
}

//Widget que plasma la informacion de los restaurantes mejores calificados
Widget _bestForm(BuildContext context) {
  final size = MediaQuery.of(context).size;
  return Scaffold(
    body: DefaultTabController(
      length: 1,
      child: Scaffold(
        body: TabBarView(
          children: <Widget>[
            ListViewBuilder(),
          ],
        ),
      ),
    ),
  );
}

//Widget que despliega la lista de los restaurantes mejores calificados
class ListViewBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(12.0),
            child: Container(
              height: 60,
              color: Colors.blueGrey,
              child: Center(
                  child: RaisedButton(
                      child: Container(
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Flexible(
                                  child: Text(
                                    mejores10[index].title,
                                    style: TextStyle(color: Colors.white),
                                    textAlign: TextAlign.left,
                                  ),
                                )
                              ],
                            ),
                            SizedBox(height: 5.0),
                            Row(
                              children: [
                                Text(
                                  'Calificación: ${mejores10[index].rating}',
                                  style: TextStyle(color: Colors.white),
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      color: Color.fromRGBO(0, 73, 141, 1.0),
                      onPressed: () {
                        rest = mejores10[index];
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RestaurantPage()));
                      })),
            ),
          );
        },
        itemCount: mejores10.length);
  }
}
