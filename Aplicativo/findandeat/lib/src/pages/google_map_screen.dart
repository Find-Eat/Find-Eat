import 'package:findandeat/providers/user_provider.dart';
import 'package:findandeat/src/models/list_favorites_model.dart';
import 'package:findandeat/src/pages/filtro_page.dart';
import 'package:findandeat/src/pages/home_page.dart';
import 'package:findandeat/src/pages/login_page.dart';
import 'package:findandeat/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:findandeat/src/models/restaurant_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';

//Variables Globales
GoogleMapController _controller;
String name = "Nombre del Lugar";

//Clase Principal de la pagina tipo Statefull
class GoogleMapScreen extends StatefulWidget {
  @override
  _GoogleMapScreenState createState() => _GoogleMapScreenState();
}

//Clase que actualiza la informacion de la clase principal
class _GoogleMapScreenState extends State<GoogleMapScreen> {
  UserProvider user;

//Funcion que establece el estado inicial
  @override
  void initState() {
    super.initState();
    user = new UserProvider();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

//Crea un mensaje que devuelve la informacion del restaurante
  Future<String> createAlertDialog(
      BuildContext context, RestaurantModel restaurant) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text(
                'Restaurante: ${restaurant.title} \nCalificacion: ${restaurant.rating} de 5 \nDireccion: ${restaurant.address} '),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Agregar a Favoritos'),
                  onPressed: () {
                    bool existe = false;
                    for (var i = 0; i < getListFav.length; i++) {
                      if (restaurant.title == getListFav[i].restaurant) {
                        existe = true;
                      }
                    }
                    if (existe == false) {
                      UserProvider userProv = new UserProvider();
                      FavoriteModel model = new FavoriteModel();
                      model.user = getUsuario.user;
                      model.restaurant = restaurant.title;
                      int status = 0;
                      Future<bool> resp =
                          userProv.addFavoriteRestaurant(model, status);
                      getListFav.add(model);
                      mensajeConfirmacion(context);
                    } else {
                      mensajeExiste(context);
                    }
                  }),
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Cancelar'),
                  onPressed: () {
                    Navigator.of(context).pop('Se ha Cancelado la operacion');
                  })
            ],
          );
        });
  }

//Llena el mapa con los marcadores que tenga registrado
  Set<Marker> llenarMarcadores() {
    var _markers = Set<Marker>();
    _markers.add(Marker(
        markerId: MarkerId('User'),
        position: getLatLong,
        infoWindow: InfoWindow(title: 'Tú Ubicación'),
        icon:
            BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueYellow)));

    setState(() {
      if (getTipoMapa == 0) {
        for (var i = 0; i < listRest.length; i++) {
          _markers.add(Marker(
              markerId: MarkerId(listRest[i].id),
              position: LatLng(double.parse(listRest[i].lat),
                  double.parse(listRest[i].long)),
              infoWindow: InfoWindow(
                title: listRest[i].title,
                snippet: 'Calificación: ${listRest[i].rating}',
              ),
              onTap: () {
                createAlertDialog(context, listRest[i]).then((onValue) {});
              }));
        }
      } else if (getTipoMapa == 1) {
        for (var i = 0; i < getListFav.length; i++) {
          RestaurantModel restaurant =
              res.obtenerRestaurant(getListFav[i].restaurant, listRest);
          _markers.add(Marker(
              markerId: MarkerId(restaurant.id),
              position: LatLng(
                  double.parse(restaurant.lat), double.parse(restaurant.long)),
              infoWindow: InfoWindow(
                title: listRest[i].title,
                snippet: 'Calificación: ${restaurant.rating}',
              ),
              onTap: () {
                createAlertDialog(context, restaurant).then((onValue) {});
              }));
        }
      } else {
        if (getListaFiltrada != null) {
          for (var i = 0; i < getListaFiltrada.length; i++) {
            _markers.add(Marker(
                markerId: MarkerId(getListaFiltrada[i].id),
                position: LatLng(double.parse(getListaFiltrada[i].lat),
                    double.parse(getListaFiltrada[i].long)),
                infoWindow: InfoWindow(
                  title: getListaFiltrada[i].title,
                  snippet: 'Calificación: ${getListaFiltrada[i].rating}',
                ),
                onTap: () {
                  createAlertDialog(context, getListaFiltrada[i])
                      .then((onValue) {});
                }));
          }
        }
      }
    });

    return _markers;
  }

//Funcion que permite mover el mapa creado
  void _onMapCreated(GoogleMapController controller) {
    controller.setMapStyle(Utils.mapStyle);
    setState(() {
      _controller = controller;
      _controller.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: getLatLong, zoom: 17.0)));
    });
  }

//Widget que despliega el mapa
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            title: Row(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => HomePage()));
                    },
                    icon: Icon(Icons.arrow_back)),
                Text(
                  'Ubica tu Restaurante',
                  textAlign: TextAlign.center,
                )
              ],
            ),
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    showSearch(context: context, delegate: _DataSearch());
                  })
            ],
            backgroundColor: Color.fromRGBO(0, 73, 141, 1.0)),
        body: GoogleMap(
          zoomControlsEnabled: false,
          initialCameraPosition: CameraPosition(target: getLatLong, zoom: 17.0),
          onMapCreated: _onMapCreated,
          markers: llenarMarcadores(),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Color.fromRGBO(0, 73, 141, 1.0),
          onPressed: () => _controller.animateCamera(
              CameraUpdate.newCameraPosition(
                  CameraPosition(target: getLatLong, zoom: 17.0))),
          child: const Icon(Icons.center_focus_strong),
        ));
  }
}

//Mensaje de confirmacion cuando se agrega un restaurante a la lista de favoritos
Future<String> mensajeConfirmacion(BuildContext context) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(''),
          content: Text('Restaurante Agregado exitosamente'),
          actions: <Widget>[
            MaterialButton(
                elevation: 5.0,
                child: Text('Cerrar Mensaje'),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => HomePage()));
                }),
          ],
        );
      });
}

//Mensaje que indica que ya existe ese restaurante dentro de la lista de favoritos
Future<String> mensajeExiste(BuildContext context) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(''),
          content: Text(
              'Ya se encuentra este restaurante dentro de tu lista de favoritos'),
          actions: <Widget>[
            MaterialButton(
                elevation: 5.0,
                child: Text('Cerrar Mensaje'),
                onPressed: () {
                  Navigator.of(context).pop('Se ha Cancelado la operacion');
                }),
          ],
        );
      });
}

//Clase que forma parte de la principal y corresponde a la barra de busqueda
class _DataSearch extends SearchDelegate<String> {
  @override
  List<Widget> buildActions(BuildContext context) {
    // Aqui se modifica las funciones de la barra de busqueda
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = '';
          })
    ];
    throw UnimplementedError();
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Aqui se colocan iconos en la barra
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () {
          close(context, null);
        });
    throw UnimplementedError();
  }

  @override
  Widget buildResults(BuildContext context) {
    // Muestra Resultados basados en la seleccion
    RestaurantModel restaurant = res.obtenerRestaurant(query, listRest);
    return Scaffold(
      body: Stack(children: <Widget>[
        _crearFondo(context),
        _restaurantForm(context, restaurant),
      ]),
    );
  }

  List<String> llenarSugerenciaFavorita(List<FavoriteModel> favoritos) {
    List<String> sugerenciasFav = [];
    for (var i = 0; i < favoritos.length; i++) {
      if (!sugerenciasFav.contains(favoritos[i].restaurant)) {
        sugerenciasFav.add(favoritos[i].restaurant);
      }
    }
    return sugerenciasFav;
  }

  List<String> llenarSugerenciaFiltros(List<RestaurantModel> restaurant) {
    List<String> sugerenciasFiltro = [];
    if (restaurant != null) {
      for (var i = 0; i < restaurant.length; i++) {
        if (!sugerenciasFiltro.contains(restaurant[i].title)) {
          sugerenciasFiltro.add(restaurant[i].title);
        }
      }
    }

    return sugerenciasFiltro;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> restaurants = suggestions;
    List<String> favorites = llenarSugerenciaFavorita(getListFav);

    // Muestra alternativas de busqueda
    if (tipoMapa == 0) {
      final suggestionList = query.isEmpty
          ? suggestions
          : restaurants
              .where((p) => p.toLowerCase().startsWith(query))
              .toList();
      return ListView.builder(
        itemBuilder: (context, index) => ListTile(
          onTap: () {
            query = suggestionList[index];
            showResults(context);
          },
          leading: Icon(Icons.restaurant),
          title: RichText(
              text: TextSpan(
                  text: suggestionList[index].substring(0, query.length),
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                  children: [
                TextSpan(
                    text: suggestionList[index].substring(query.length),
                    style: TextStyle(color: Colors.grey))
              ])),
        ),
        itemCount: suggestionList.length,
      );
    } else if (tipoMapa == 1) {
      final suggestionList = query.isEmpty
          ? llenarSugerenciaFavorita(getListFav)
          : favorites.where((p) => p.toLowerCase().startsWith(query)).toList();
      return ListView.builder(
        itemBuilder: (context, index) => ListTile(
          onTap: () {
            query = suggestionList[index];
            showResults(context);
          },
          leading: Icon(Icons.restaurant),
          title: RichText(
              text: TextSpan(
                  text: suggestionList[index].substring(0, query.length),
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                  children: [
                TextSpan(
                    text: suggestionList[index].substring(query.length),
                    style: TextStyle(color: Colors.grey))
              ])),
        ),
        itemCount: suggestionList.length,
      );
    } else {
      List<String> filtrados = llenarSugerenciaFiltros(getListaFiltrada);
      final suggestionList = query.isEmpty
          ? llenarSugerenciaFiltros(getListaFiltrada)
          : filtrados.where((p) => p.toLowerCase().startsWith(query)).toList();
      return ListView.builder(
        itemBuilder: (context, index) => ListTile(
          onTap: () {
            query = suggestionList[index];
            showResults(context);
          },
          leading: Icon(Icons.restaurant),
          title: RichText(
              text: TextSpan(
                  text: suggestionList[index].substring(0, query.length),
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                  children: [
                TextSpan(
                    text: suggestionList[index].substring(query.length),
                    style: TextStyle(color: Colors.grey))
              ])),
        ),
        itemCount: suggestionList.length,
      );
    }

    throw UnimplementedError();
  }

//Widget que crea el fondo
  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final fondodegradado = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Color.fromRGBO(0, 73, 141, 1.0),
        Color.fromRGBO(90, 70, 178, 1.0)
      ])),
    );

    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          color: Color.fromRGBO(255, 255, 255, 0.05)),
    );

    return Stack(
      children: <Widget>[
        fondodegradado,
        Positioned(top: 90.0, left: 30.0, child: circulo),
        Positioned(top: -40.0, right: -30.0, child: circulo),
        Positioned(bottom: -50.0, right: -10.0, child: circulo),
        Positioned(bottom: 120.0, right: 20.0, child: circulo),
        Positioned(bottom: -50.0, left: -20.0, child: circulo),
        Container(
          padding: EdgeInsets.only(top: 30.0),
          child: Column(
            children: <Widget>[
              Icon(Icons.food_bank, color: Colors.white, size: 100.0),
              SizedBox(height: 10.0, width: double.infinity),
              Text('FIND & EAT',
                  style: TextStyle(color: Colors.white, fontSize: 25.0))
            ],
          ),
        )
      ],
    );
  }

  double obtenerCalificacionSA(RestaurantModel restaurant) {
    double calificacion = 0.0;
    for (var i = 0; i < getListCalificada.length; i++) {
      if (getListCalificada[i].id == restaurant.id) {
        calificacion = double.parse(getListCalificada[i].rating);
      }
    }
    return calificacion;
  }

//Widget que plasma la informacion del restaurante
  Widget _restaurantForm(BuildContext context, RestaurantModel restaurant) {
    final size = MediaQuery.of(context).size;
    double calSA = obtenerCalificacionSA(restaurant);
    if (restaurant == null) {
      return SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SafeArea(
              child: Container(
                height: 180.0,
              ),
            ),
            Container(
              width: size.width * 0.85,
              margin: EdgeInsets.symmetric(vertical: 30.0),
              padding: EdgeInsets.symmetric(vertical: 50.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.black26,
                        blurRadius: 3.0,
                        offset: Offset(0.0, 5.0),
                        spreadRadius: 3.0)
                  ]),
              child: Column(
                children: <Widget>[
                  Text('Restaurante no Encontrado',
                      style: TextStyle(fontSize: 20.0)),
                  SizedBox(height: 30.0),
                  _regresarAMapa(context, restaurant)
                ],
              ),
            ),
            SizedBox(height: 100.0)
          ],
        ),
      );
    } else {
      return SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SafeArea(
              child: Container(
                height: 180.0,
              ),
            ),
            Container(
              width: size.width * 0.85,
              margin: EdgeInsets.symmetric(vertical: 30.0),
              padding: EdgeInsets.symmetric(vertical: 50.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.black26,
                        blurRadius: 3.0,
                        offset: Offset(0.0, 5.0),
                        spreadRadius: 3.0)
                  ]),
              child: Column(
                children: <Widget>[
                  Text('Datos del Restaurante',
                      style: TextStyle(fontSize: 20.0)),
                  SizedBox(height: 60.0),
                  Text(restaurant.title),
                  SizedBox(height: 30.0),
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                    Text("Calificacion: ${restaurant.rating} "),
                    Image.asset(
                      'assets/icongoogle.png',
                      height: 30,
                      width: 30,
                    )
                  ]),
                  SizedBox(height: 30.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Calificacion: ${calSA.toStringAsPrecision(2)} "),
                      Image.asset(
                        'assets/icontwitter.png',
                        height: 30,
                        width: 30,
                      )
                    ],
                  ),
                  SizedBox(height: 30.0),
                  Text("Direccion: ${restaurant.address} "),
                  SizedBox(height: 30.0),
                  _agregarFavoritos(context, restaurant),
                  SizedBox(height: 30.0),
                  _verEnMapa(context, restaurant)
                ],
              ),
            ),
            SizedBox(height: 100.0)
          ],
        ),
      );
    }
  }

//Widget que agrega un restaurante a favoritos dentro del panel del restaurante
  Widget _agregarFavoritos(BuildContext context, RestaurantModel restaurant) {
    return RaisedButton(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
          child: Text('Agregar a Favoritos'),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        elevation: 0.0,
        color: Color.fromRGBO(0, 73, 141, 1.0),
        textColor: Colors.white,
        onPressed: () {
          bool existe = false;
          for (var i = 0; i < getListFav.length; i++) {
            if (restaurant.title == getListFav[i].restaurant) {
              existe = true;
            }
          }
          if (existe == false) {
            UserProvider userProv = new UserProvider();
            FavoriteModel model = new FavoriteModel();
            model.user = getUsuario.user;
            model.restaurant = restaurant.title;
            int status = 0;
            Future<bool> resp = userProv.addFavoriteRestaurant(model, status);
            getListFav.add(model);
            mensajeConfirmacion(context);
          } else {
            mensajeExiste(context);
          }
        });
  }

//Widget que devuelve a la pantalla principal del mapa
  Widget _regresarAMapa(BuildContext context, RestaurantModel restaurant) {
    return RaisedButton(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
          child: Text('Regresar'),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        elevation: 0.0,
        color: Color.fromRGBO(0, 73, 141, 1.0),
        textColor: Colors.white,
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => GoogleMapScreen()));
        });
  }

//Widget que ubica el restaurante dentro del mapa
  Widget _verEnMapa(BuildContext context, RestaurantModel restaurant) {
    return RaisedButton(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 18.0),
          child: Text('Ubicar Restaurantes'),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        elevation: 0.0,
        color: Color.fromRGBO(0, 73, 141, 1.0),
        textColor: Colors.white,
        onPressed: () {
          _controller.animateCamera(
            CameraUpdate.newCameraPosition(CameraPosition(
                target: LatLng(double.parse(restaurant.lat),
                    double.parse(restaurant.long)),
                zoom: 30.0)),
          );
          close(context, null);
        });
  }
}
