import 'dart:convert';
import 'package:findandeat/providers/rest_provider.dart';
import 'package:findandeat/providers/user_provider.dart';
import 'package:findandeat/src/models/list_favorites_model.dart';
import 'package:findandeat/src/models/restaurant_model.dart';
import 'package:findandeat/src/models/user_model.dart';
import 'package:findandeat/src/pages/home_page.dart';
import 'package:findandeat/src/pages/login_page.dart';
import 'package:findandeat/src/pages/login_validation.dart';
import 'package:flutter/material.dart';
import 'package:findandeat/src/bloc/provider.dart';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:crypto/crypto.dart';

//Variables Globales

bool validation = false;
bool get getVal => validation;
const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
Random _rnd = Random();
var clave;
String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
    length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

//Clase Principal de tipo statefull
class ValidarEmailPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ValidarEmail();
  }
}

//Clase que cambia el estado de la clase principal
class ValidarEmail extends State<ValidarEmailPage> {
  UserModel _usuarioValidate = new UserModel();
  String _emailLoginValidate = '';
  String _passwordLogin = '';
  UserProvider user;
  bool isApiCallProcess = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return LoginVal(
      child: _uiSetUp(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  @override
  Widget _uiSetUp(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        _crearFondo(context),
        _loginForm(context),
      ],
    ));
  }

  //Widget que plasma la informacion del login
  Widget _loginForm(BuildContext context) {
    final bloc = Provider.of(context);
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
        child: Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 180.0,
            ),
          ),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 30.0),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 3.0,
                      offset: Offset(0.0, 5.0),
                      spreadRadius: 3.0)
                ]),
            child: Column(
              children: <Widget>[
                Text('VALIDACIÓN', style: TextStyle(fontSize: 20.0)),
                SizedBox(height: 60.0),
                Text('Ingresa tu correo con el que te registraste en Find&Eat'),
                SizedBox(height: 30.0),
                _crearEmail(),
                SizedBox(height: 30.0),
                _crearBotonLogin(bloc),
                SizedBox(height: 30.0),
                _crearBotonCancelar(bloc),
                SizedBox(height: 30.0),
              ],
            ),
          ),
        ],
      ),
    ));
  }

  //Widget que crea el campo para ingresar el email
  Widget _crearEmail() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: TextFormField(
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            icon: Icon(Icons.alternate_email, color: Colors.blueGrey),
            hintText: 'ejemplo@dominio.com',
            labelText: 'Correo Electronico',
          ),
          maxLength: 50,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Se requiere llenar este campo';
            }

            Pattern pattern =
                r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

            if (!RegExp(pattern).hasMatch(value)) {
              return 'Debe ingresar un email valido';
            }

            return null;
          },
          onSaved: (String value) {
            _emailLoginValidate = value;
            _usuarioValidate.user = value;
            print(_usuarioValidate.user);
          },
        ));
  }

  //Widget que crea el boton para ingresar al HomePage
  Widget _crearBotonLogin(LoginBloc bloc) {
    return FutureBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Enviar'),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 0.0,
            color: Color.fromRGBO(0, 73, 141, 1.0),
            textColor: Colors.white,
            onPressed: () {
              if (validateAndSave()) {
                setState(() {
                  isApiCallProcess = true;
                });
                int statusCode = 0;
                var bytes;
                var digest;
                UserProvider userProv = new UserProvider();
                userProv
                    .validarCambioClaveUsuario(
                        validation, _emailLoginValidate, _usuarioValidate)
                    .then((value) => {
                          setState(() {
                            isApiCallProcess = false;
                          }),
                          if (_usuarioValidate.name != null)
                            {
                              clave = getRandomString(10),
                              bytes = utf8.encode(clave),
                              digest = sha256.convert(bytes),
                              _usuarioValidate.pass = digest.toString(),
                              _usuarioValidate.name =
                                  '#' + _usuarioValidate.name,
                              print(_usuarioValidate.pass),
                              print(_usuarioValidate.name),
                              userProv.modifyUser(_usuarioValidate, statusCode),
                              createAlertDialogValido(context)
                            }
                          else
                            {createAlertDialogInvalido(context)}
                        });
              }
            });
      },
    );
  }

  //Funcion que envia al email la clave temporal del usuario
  Future sendEmail(String email, String password) async {
    final serviceId = 'service_0luc2ug';
    final templateId = 'template_p0zpejl';
    final userId = 'user_JvFtxpTKFfJ54xAKi8QHe';
    final url = Uri.parse('https://api.emailjs.com/api/v1.0/email/send');
    final response = await http.post(url,
        headers: {
          'origin': 'http://localhost',
          'Content-Type': 'application/json'
        },
        body: json.encode({
          'service_id': serviceId,
          'template_id': templateId,
          'user_id': userId,
          'template_params': {
            'username': email,
            'user_subject': 'Find&Eat Clave Temporal',
            'password': password
          }
        }));
    print(response.body);
  }

  //Widget que confirma el inicio de sesion
  Future<String> createAlertDialogValido(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text(
                'Se ha enviado una clave temporal a tu correo electronico'),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Cerrar Mensaje'),
                  onPressed: () {
                    sendEmail(_usuarioValidate.user, clave);
                    _usuarioValidate = new UserModel();
                    _emailLoginValidate = '';
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  }),
            ],
          );
        });
  }

  //Widget que invalida el inicio de sesion
  Future<String> createAlertDialogInvalido(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text('Por Favor Verifique sus datos'),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Cerrar Mensaje'),
                  onPressed: () {
                    _usuarioValidate = new UserModel();
                    _emailLoginValidate = '';
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ValidarEmailPage()));
                  }),
            ],
          );
        });
  }

  //Widget que direcciona a la pagina de registro
  Widget _crearBotonCancelar(LoginBloc bloc) {
    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Regresar'),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 0.0,
            color: Color.fromRGBO(0, 73, 141, 1.0),
            textColor: Colors.white,
            onPressed: () => Navigator.push(
                context, MaterialPageRoute(builder: (context) => LoginPage())));
      },
    );
  }

  //Funcion que valida los datos ingresados
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

//Widget que crea el fondo
  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final fondodegradado = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Color.fromRGBO(0, 73, 141, 1.0),
        Color.fromRGBO(90, 70, 178, 1.0)
      ])),
    );

    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          color: Color.fromRGBO(255, 255, 255, 0.05)),
    );

    return Stack(
      children: <Widget>[
        fondodegradado,
        Positioned(top: 90.0, left: 30.0, child: circulo),
        Positioned(top: -40.0, right: -30.0, child: circulo),
        Positioned(bottom: -50.0, right: -10.0, child: circulo),
        Positioned(bottom: 120.0, right: 20.0, child: circulo),
        Positioned(bottom: -50.0, left: -20.0, child: circulo),
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Icon(Icons.food_bank, color: Colors.white, size: 100.0),
              SizedBox(height: 10.0, width: double.infinity),
              Text('FIND & EAT',
                  style: TextStyle(color: Colors.white, fontSize: 25.0))
            ],
          ),
        )
      ],
    );
  }
}
