import 'package:findandeat/src/models/user_model.dart';
import 'package:findandeat/src/pages/filtro_page.dart';
import 'package:findandeat/src/pages/listado_favoritos.dart';
import 'package:findandeat/src/pages/listado_mejores.dart';
import 'package:findandeat/src/pages/login_page.dart';
import 'package:findandeat/src/pages/modificar_usuario_page.dart';
import 'package:flutter/material.dart';
import 'package:findandeat/src/bloc/provider.dart';
import 'package:findandeat/src/pages/google_map_screen.dart';

int tipoMapa;
int get getTipoMapa => tipoMapa;

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return Home();
  }
}

//Clase Principal tipo Stateless
class Home extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text('Bienvenido'),
          actions: <Widget>[
            IconButton(
                icon: Text("Log Off"),
                onPressed: () {
                  setUsuario = new UserModel();
                  setListFav = new List.empty(growable: true);
                  setPermisoMapa = false;
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LoginPage()));
                })
          ],
          backgroundColor: Color.fromRGBO(0, 73, 141, 1.0)),
      body: Stack(children: <Widget>[
        _crearFondo(context),
        _userForm(context),
      ]),
    );
  }
}

//Crea un mensaje que devuelve la informacion del restaurante
Future<String> createAlertDialog(BuildContext context) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(''),
          content: Text('¿Permitir que Find&Eat obtenga su ubicación?'),
          actions: <Widget>[
            MaterialButton(
                elevation: 5.0,
                child: Text('Sí'),
                onPressed: () {
                  print(permisoMapa);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GoogleMapScreen()));
                }),
            MaterialButton(
                elevation: 5.0,
                child: Text('No'),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GoogleMapScreen()));
                })
          ],
        );
      });
}

//Widget que crea el fondo
Widget _crearFondo(BuildContext context) {
  final size = MediaQuery.of(context).size;

  final fondodegradado = Container(
    height: size.height * 0.4,
    width: double.infinity,
    decoration: BoxDecoration(
        gradient: LinearGradient(colors: <Color>[
      Color.fromRGBO(0, 73, 141, 1.0),
      Color.fromRGBO(90, 70, 178, 1.0)
    ])),
  );

  final circulo = Container(
    width: 100.0,
    height: 100.0,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(255, 255, 255, 0.05)),
  );

  return Stack(
    children: <Widget>[
      fondodegradado,
      Positioned(top: 90.0, left: 30.0, child: circulo),
      Positioned(top: -40.0, right: -30.0, child: circulo),
      Positioned(bottom: -50.0, right: -10.0, child: circulo),
      Positioned(bottom: 120.0, right: 20.0, child: circulo),
      Positioned(bottom: -50.0, left: -20.0, child: circulo),
      Container(
        padding: EdgeInsets.only(top: 30.0),
        child: Column(
          children: <Widget>[
            Icon(Icons.food_bank, color: Colors.white, size: 100.0),
            SizedBox(height: 10.0, width: double.infinity),
            Text('FIND & EAT',
                style: TextStyle(color: Colors.white, fontSize: 25.0))
          ],
        ),
      )
    ],
  );
}

//Widget que plasma la informacion del usuario y las opciones disponibles
Widget _userForm(BuildContext context) {
  final size = MediaQuery.of(context).size;

  return SingleChildScrollView(
    child: Column(
      children: <Widget>[
        SafeArea(
          child: Container(
            height: 180.0,
          ),
        ),
        Container(
          width: size.width * 0.85,
          margin: EdgeInsets.symmetric(vertical: 30.0),
          padding: EdgeInsets.symmetric(vertical: 50.0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black26,
                    blurRadius: 3.0,
                    offset: Offset(0.0, 5.0),
                    spreadRadius: 3.0)
              ]),
          child: Column(
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Datos del Usuario', style: TextStyle(fontSize: 20.0)),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ModificarInfoScreen()));
                    },
                    child: Icon(Icons.edit, color: Colors.white),
                    style: ElevatedButton.styleFrom(
                      shape: CircleBorder(),
                      padding: EdgeInsets.all(10),
                      primary: Color.fromRGBO(0, 73, 141, 1.0),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 60.0),
              Text("Nombre: ${getUsuario.name} "),
              SizedBox(height: 30.0),
              Text("Email: ${getUsuario.user} "),
              SizedBox(height: 30.0),
              _verRestaurantesFavoritos(context),
              SizedBox(height: 30.0),
              _verMejoresRestaurantes(context),
              SizedBox(height: 30.0),
              _verMapaRestaurantes(context),
              SizedBox(height: 30.0),
              _verMapaFavoritos(context),
              SizedBox(height: 30.0),
              _verMapaXFiltro(context)
            ],
          ),
        ),
        SizedBox(height: 100.0)
      ],
    ),
  );
}

//Widget que cre al boton para ver restaurantes favoritos
Widget _verRestaurantesFavoritos(BuildContext context) {
  return RaisedButton(
      child: Container(
        height: 45,
        width: 300,
        padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
        child: Text('Lista de Favoritos'),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 0.0,
      color: Color.fromRGBO(0, 73, 141, 1.0),
      textColor: Colors.white,
      onPressed: () {
        print(getListCalificada.length);
        res.juntarCalificacion(listRest, getListCalificada);
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => FavoritePage()));
      });
}

//Widget que crea el boton para ver los restaurantes mejores calificados
Widget _verMejoresRestaurantes(BuildContext context) {
  return RaisedButton(
      child: Container(
        height: 45,
        width: 300,
        padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
        child: Text('Mejores Calificados'),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 0.0,
      color: Color.fromRGBO(0, 73, 141, 1.0),
      textColor: Colors.white,
      onPressed: () {
        res.juntarCalificacion(listRest, getListCalificada);
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => BestRestaurantsPage()));
      });
}

//Widget que carga y plasma el mapa con todos los restaurantes
Widget _verMapaRestaurantes(BuildContext context) {
  return RaisedButton(
      child: Container(
        height: 45,
        width: 300,
        padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
        child: Text('Mapa Restaurantes'),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 0.0,
      color: Color.fromRGBO(0, 73, 141, 1.0),
      textColor: Colors.white,
      onPressed: () {
        res.juntarCalificacion(listRest, getListCalificada);
        print(getLatLong);
        tipoMapa = 0;
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => GoogleMapScreen()));
      });
}

//Widget que carga y plasma el mapa con todos los restaurantes
Widget _verMapaFavoritos(BuildContext context) {
  return RaisedButton(
      child: Container(
        height: 45,
        width: 300,
        padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
        child: Text('Mapa de Favoritos'),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 0.0,
      color: Color.fromRGBO(0, 73, 141, 1.0),
      textColor: Colors.white,
      onPressed: () {
        res.juntarCalificacion(listRest, getListCalificada);
        print(getLatLong);
        tipoMapa = 1;
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => GoogleMapScreen()));
      });
}

//Widget que carga y plasma el mapa con todos los restaurantes
Widget _verMapaXFiltro(BuildContext context) {
  return RaisedButton(
      child: Container(
        height: 45,
        width: 300,
        padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
        child: Text('Mapa por Calificación'),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 0.0,
      color: Color.fromRGBO(0, 73, 141, 1.0),
      textColor: Colors.white,
      onPressed: () {
        res.juntarCalificacion(listRest, getListCalificada);
        print(getLatLong);
        tipoMapa = 2;
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => FiltroScreen()));
      });
}
