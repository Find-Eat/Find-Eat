import 'package:findandeat/providers/user_provider.dart';
import 'package:findandeat/src/models/list_favorites_model.dart';
import 'package:findandeat/src/models/restaurant_model.dart';
import 'package:findandeat/src/pages/home_page.dart';
import 'package:findandeat/src/pages/listado_comentarios.dart';
import 'package:findandeat/src/pages/listado_favoritos.dart';
import 'package:findandeat/src/pages/login_page.dart';
import 'package:findandeat/src/pages/restaurant_page.dart';
import 'package:flutter/material.dart';
import 'package:findandeat/src/bloc/provider.dart';

//Clase Principal de la Pagina tipo stateless
class FavoriteRestaurantPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    return Scaffold(
      appBar: AppBar(
          title: Text('Información del restaurante'),
          backgroundColor: Color.fromRGBO(0, 73, 141, 1.0)),
      body: Stack(children: <Widget>[
        _crearFondo(context),
        _restaurantForm(context, getFavRest),
      ]),
    );
  }
}

//Widget designado a crear el Fondo
Widget _crearFondo(BuildContext context) {
  final size = MediaQuery.of(context).size;

  final fondodegradado = Container(
    height: size.height * 0.4,
    width: double.infinity,
    decoration: BoxDecoration(
        gradient: LinearGradient(colors: <Color>[
      Color.fromRGBO(0, 73, 141, 1.0),
      Color.fromRGBO(90, 70, 178, 1.0)
    ])),
  );
  final circulo = Container(
    width: 100.0,
    height: 100.0,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(255, 255, 255, 0.05)),
  );
  return Stack(
    children: <Widget>[
      fondodegradado,
      Positioned(top: 90.0, left: 30.0, child: circulo),
      Positioned(top: -40.0, right: -30.0, child: circulo),
      Positioned(bottom: -50.0, right: -10.0, child: circulo),
      Positioned(bottom: 120.0, right: 20.0, child: circulo),
      Positioned(bottom: -50.0, left: -20.0, child: circulo),
      Container(
        padding: EdgeInsets.only(top: 30.0),
        child: Column(
          children: <Widget>[
            Icon(Icons.food_bank, color: Colors.white, size: 100.0),
            SizedBox(height: 10.0, width: double.infinity),
            Text('FIND & EAT',
                style: TextStyle(color: Colors.white, fontSize: 25.0))
          ],
        ),
      )
    ],
  );
}

double obtenerCalificacionSA(RestaurantModel restaurant) {
  double calificacion = 0.0;
  for (var i = 0; i < getListCalificada.length; i++) {
    if (getListCalificada[i].id == restaurant.id) {
      calificacion = double.parse(getListCalificada[i].rating);
    }
  }
  return calificacion;
}

//Widget que plasma la informacion del restaurante
Widget _restaurantForm(BuildContext context, RestaurantModel restaurant) {
  final size = MediaQuery.of(context).size;
  double calSA = obtenerCalificacionSA(restaurant);
  return SingleChildScrollView(
    child: Column(
      children: <Widget>[
        SafeArea(
          child: Container(
            height: 180.0,
          ),
        ),
        Container(
          width: size.width * 0.85,
          margin: EdgeInsets.symmetric(vertical: 30.0),
          padding: EdgeInsets.symmetric(vertical: 50.0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black26,
                    blurRadius: 3.0,
                    offset: Offset(0.0, 5.0),
                    spreadRadius: 3.0)
              ]),
          child: Column(
            children: <Widget>[
              Text('Datos del Restaurante', style: TextStyle(fontSize: 20.0)),
              SizedBox(height: 60.0),
              Text(restaurant.title),
              SizedBox(height: 30.0),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text("Calificacion: ${restaurant.rating} "),
                Image.asset(
                  'assets/icongoogle.png',
                  height: 30,
                  width: 30,
                )
              ]),
              SizedBox(height: 30.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Calificacion: ${calSA.toStringAsPrecision(2)} "),
                  Image.asset(
                    'assets/icontwitter.png',
                    height: 30,
                    width: 30,
                  )
                ],
              ),
              SizedBox(height: 30.0),
              Text("Direccion: ${restaurant.address} "),
              SizedBox(height: 30.0),
              _verComentarios(context, restaurant),
              SizedBox(height: 30.0),
              _eliminarFavoritos(context, restaurant),
            ],
          ),
        ),
        SizedBox(height: 100.0)
      ],
    ),
  );
}

//Widget que agrega un restanruante al listado de favoritos
Widget _verComentarios(BuildContext context, RestaurantModel restaurant) {
  return RaisedButton(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
        child: Text('Comentarios'),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 0.0,
      color: Color.fromRGBO(0, 73, 141, 1.0),
      textColor: Colors.white,
      onPressed: () {
        if (restaurant.tweets != null) {
          setListTweets = restaurant.tweets;
        } else {
          setListTweets = [];
        }
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => CommentsPage()));
      });
}

//Widget que elimina un restaurante de la lista de favoritos
Widget _eliminarFavoritos(BuildContext context, RestaurantModel restaurant) {
  return RaisedButton(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
        child: Text('Eliminar de Favoritos'),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 0.0,
      color: Color.fromRGBO(0, 73, 141, 1.0),
      textColor: Colors.white,
      onPressed: () {
        UserProvider userProv = new UserProvider();
        FavoriteModel model = new FavoriteModel();
        model.user = getUsuario.user;
        model.restaurant = restaurant.title;
        int status = 0;
        getListFav.removeWhere((item) => item.restaurant == restaurant.title);
        userProv.deleteFavoriteRestaurant(model, status);
        confirmarEliminar(context, model);
      });
}

//Mensaje de confirmacion al eliminar el restaurante
Future<String> confirmarEliminar(BuildContext context, FavoriteModel favorito) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(''),
          content: Text('Se ha eliminado ${favorito.restaurant} con exito'),
          actions: <Widget>[
            MaterialButton(
                elevation: 5.0,
                child: Text('Cerrar Mensaje'),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => HomePage()));
                }),
          ],
        );
      });
}
