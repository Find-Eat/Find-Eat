import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:findandeat/providers/user_provider.dart';
import 'package:findandeat/src/models/user_model.dart';
import 'package:findandeat/src/pages/login_validation.dart';
import 'package:flutter/material.dart';
import 'package:findandeat/src/bloc/provider.dart';
import 'package:findandeat/src/pages/login_page.dart';
import 'package:http/http.dart' as http;

//Variables Globales
int statusCode;
set setCode(int value) {
  statusCode = value;
}

//Clase principal de tipo statefull
class CambioClaveScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return Cambio();
  }
}

//Clase que cambia la informacion de la clase principal
class Cambio extends State<CambioClaveScreen> {
  String _name = '';
  String _email = '';
  TextEditingController _newPassword = TextEditingController();
  TextEditingController _rnewPassword = TextEditingController();
  UserModel usuarioARegistar = new UserModel();
  UserProvider user;
  bool isApiCallProcessRegister = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return LoginVal(
      child: _uiRegister(context),
      inAsyncCall: isApiCallProcessRegister,
      opacity: 0.3,
    );
  }

  @override
  //Widget que acopla la informacion
  Widget _uiRegister(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        _crearFondo(context),
        _cambioClaveForm(context),
      ],
    ));
  }

  //Widget que muestra los campos para registrarse
  Widget _cambioClaveForm(BuildContext context) {
    final bloc = Provider.of(context);
    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        child: Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 180.0,
            ),
          ),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 30.0),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 3.0,
                      offset: Offset(0.0, 5.0),
                      spreadRadius: 3.0)
                ]),
            child: Column(
              children: <Widget>[
                Text('Cambia tu clave', style: TextStyle(fontSize: 20.0)),
                SizedBox(height: 60.0),
                SizedBox(height: 30.0),
                _crearPassword(),
                SizedBox(height: 30.0),
                _repetirPassword(),
                SizedBox(height: 30.0),
                _confirmarCambio(bloc),
                SizedBox(height: 30.0),
                _cancelarCambio(bloc),
              ],
            ),
          ),
          SizedBox(height: 100.0)
        ],
      ),
    ));
  }

  //Widget que crea el campo del Password
  Widget _crearPassword() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: TextFormField(
          controller: _newPassword,
          obscureText: true,
          decoration: InputDecoration(
            icon: Icon(Icons.lock_outline, color: Colors.blueGrey),
            hintText: 'Ejemplo88!',
            labelText: 'Contraseña',
          ),
          maxLength: 20,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Se requiere ingresar la Contraseña';
            }
            if (value.length < 8) {
              return 'Mínimo 8 caracteres';
            }
            Pattern pattern3 =
                r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]{8,}$';
            if (!RegExp(pattern3).hasMatch(value)) {
              return 'Mínimo 1 numero, 1 Mayúscula, 1 Minúscula, y un caracter especial';
            }
            return null;
          },
          onSaved: (String value) {
            _newPassword.text = value;
            var bytes1 = utf8.encode(value);
            var digest1 = sha256.convert(bytes1);
            usuarioLogin.pass = digest1.toString();
            usuarioLogin.state = 1;
          },
        ));
  }

  //Widget que crea el campo del Password para repetirlo
  Widget _repetirPassword() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: TextFormField(
          controller: _rnewPassword,
          obscureText: true,
          decoration: InputDecoration(
            icon: Icon(Icons.lock_outline, color: Colors.blueGrey),
            hintText: 'Ingrese su Contraseña',
            labelText: 'Repita su Contraseña',
          ),
          maxLength: 20,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Se requiere ingresar la Contraseña';
            }
            if (_newPassword.text != _rnewPassword.text) {
              return 'Las contraseñas deben coincidir';
            }
            return null;
          },
        ));
  }

  //Widget que confirma el registro
  Widget _confirmarCambio(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Confirmar Datos'),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 0.0,
            color: Color.fromRGBO(0, 73, 141, 1.0),
            textColor: Colors.white,
            onPressed: () {
              if (validateAndSave()) {
                setState(() {
                  isApiCallProcessRegister = true;
                });
                int status = 0;
                usuarioLogin.name = usuarioLogin.name.replaceAll("#", "");
                UserProvider userProv = new UserProvider();
                userProv.modifyUser(usuarioLogin, status).then((value) => {
                      setState(() {
                        isApiCallProcessRegister = false;
                      }),
                    });

                createAlertDialogValido(context);
              }
            });
      },
    );
  }

  //Funcion que envia al email los datos del usuario
  Future sendEmail(String name, String email, String password) async {
    final serviceId = 'service_0luc2ug';
    final templateId = 'template_p2kiq2n';
    final userId = 'user_JvFtxpTKFfJ54xAKi8QHe';
    final url = Uri.parse('https://api.emailjs.com/api/v1.0/email/send');
    final response = await http.post(url,
        headers: {
          'origin': 'http://localhost',
          'Content-Type': 'application/json'
        },
        body: json.encode({
          'service_id': serviceId,
          'template_id': templateId,
          'user_id': userId,
          'template_params': {
            'username': email,
            'name': name,
            'user_subject': 'Find&Eat Cambio de clave exitoso',
            'password': password
          }
        }));
  }

  //Funcion Future que valida la creacion del usuario
  Future<String> createAlertDialogValido(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content:
                Text('Clave Modificada con exito, vuelva a iniciar sesión'),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Cerrar Mensaje'),
                  onPressed: () {
                    sendEmail(usuarioLogin.name, usuarioLogin.user,
                        _newPassword.text);
                    usuarioLogin = new UserModel();
                    _newPassword = new TextEditingController();
                    _rnewPassword = new TextEditingController();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  }),
            ],
          );
        });
  }

  //Funcion Future que invalida la creacion del usuario
  Future<String> createAlertDialogInvalido(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text('No se pudo cambiar la contraseña'),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Cerrar Mensaje'),
                  onPressed: () {
                    usuarioLogin = new UserModel();
                    _newPassword = new TextEditingController();
                    _rnewPassword = new TextEditingController();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  }),
            ],
          );
        });
  }

  //Funcion que valida los campos
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  //Widget que crea el boton y cancela la operacion de registro
  Widget _cancelarCambio(LoginBloc bloc) {
    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Cancelar'),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 0.0,
            color: Color.fromRGBO(0, 73, 141, 1.0),
            textColor: Colors.white,
            onPressed: () => Navigator.push(
                context, MaterialPageRoute(builder: (context) => LoginPage())));
      },
    );
  }

  //Widget que crea el fondo
  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final fondodegradado = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Color.fromRGBO(0, 73, 141, 1.0),
        Color.fromRGBO(90, 70, 178, 1.0)
      ])),
    );
    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          color: Color.fromRGBO(255, 255, 255, 0.05)),
    );
    return Stack(
      children: <Widget>[
        fondodegradado,
        Positioned(top: 90.0, left: 30.0, child: circulo),
        Positioned(top: -40.0, right: -30.0, child: circulo),
        Positioned(bottom: -50.0, right: -10.0, child: circulo),
        Positioned(bottom: 120.0, right: 20.0, child: circulo),
        Positioned(bottom: -50.0, left: -20.0, child: circulo),
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Icon(Icons.food_bank, color: Colors.white, size: 100.0),
              SizedBox(height: 10.0, width: double.infinity),
              Text('FIND & EAT',
                  style: TextStyle(color: Colors.white, fontSize: 25.0))
            ],
          ),
        )
      ],
    );
  }
}
