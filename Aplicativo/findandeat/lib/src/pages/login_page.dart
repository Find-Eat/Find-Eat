import 'dart:convert';
import 'package:findandeat/providers/rest_provider.dart';
import 'package:findandeat/providers/user_provider.dart';
import 'package:findandeat/src/models/calification_model.dart';
import 'package:findandeat/src/models/list_favorites_model.dart';
import 'package:findandeat/src/models/restaurant_model.dart';
import 'package:findandeat/src/models/user_model.dart';
import 'package:findandeat/src/pages/cambiar_clave.dart';
import 'package:findandeat/src/pages/home_page.dart';
import 'package:findandeat/src/pages/login_validation.dart';
import 'package:findandeat/src/pages/validar_correo.dart';
import 'package:flutter/material.dart';
import 'package:findandeat/src/bloc/provider.dart';
import 'package:findandeat/src/pages/register.dart';
import 'package:crypto/crypto.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

//Variables Globales
RestaurantProvider res = new RestaurantProvider();
List<String> _suggestions = [];
List<RestaurantModel> _listRest = [];
List<FavoriteModel> _listFav = [];
List<FavoriteModel> get getListFav => _listFav;
List<CalificationModel> _listCalificada = [];
List<CalificationModel> get getListCalificada => _listCalificada;
List<RestaurantModel> get listRest => _listRest;
List<String> get suggestions => _suggestions;
List<RestaurantModel> _mejores10 = [];
List<RestaurantModel> get mejores10 => _mejores10;
Digest digest1;
UserModel usuarioLogin = new UserModel();
UserModel get getUsuario => usuarioLogin;
set setUsuario(UserModel value) {
  usuarioLogin = value;
}

bool permisoMapa = false;
set setPermisoMapa(bool value) {
  permisoMapa = value;
}

bool get getPermisoMapa => permisoMapa;
LatLng latlong;
LatLng get getLatLong => latlong;
set setLangLong(LatLng value) {
  latlong = value;
}

set setListFav(List<FavoriteModel> value) {
  _listFav = value;
}

bool validation = false;
bool get getVal => validation;

//Clase Principal de tipo statefull
class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return Login();
  }
}

//Clase que cambia el estado de la clase principal
class Login extends State<LoginPage> {
  String _emailLogin = '';
  String _passwordLogin = '';
  UserProvider user;
  bool isApiCallProcess = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return LoginVal(
      child: _uiSetUp(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  @override
  Widget _uiSetUp(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        _crearFondo(context),
        _loginForm(context),
      ],
    ));
  }

  //Widget que plasma la informacion del login
  Widget _loginForm(BuildContext context) {
    final bloc = Provider.of(context);
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
        child: Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 180.0,
            ),
          ),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 30.0),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 3.0,
                      offset: Offset(0.0, 5.0),
                      spreadRadius: 3.0)
                ]),
            child: Column(
              children: <Widget>[
                Text('Ingreso', style: TextStyle(fontSize: 20.0)),
                SizedBox(height: 60.0),
                _crearEmail(),
                SizedBox(height: 30.0),
                _crearPassword(),
                SizedBox(height: 30.0),
                _crearBotonLogin(bloc),
                SizedBox(height: 30.0),
                _crearBotonRegistro(bloc),
                SizedBox(height: 30.0),
                TextButton(
                  style: TextButton.styleFrom(
                      textStyle: const TextStyle(fontSize: 15),
                      primary: Colors.grey),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ValidarEmailPage()));
                  },
                  child: const Text('¿Olvidé mi contraseña?'),
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }

  //Widget que crea el campo para ingresar el email
  Widget _crearEmail() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: TextFormField(
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            icon: Icon(Icons.alternate_email, color: Colors.blueGrey),
            hintText: 'ejemplo@dominio.com',
            labelText: 'Correo Electronico',
          ),
          maxLength: 50,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Se requiere llenar este campo';
            }

            Pattern pattern =
                r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

            if (!RegExp(pattern).hasMatch(value)) {
              return 'Debe ingresar un email valido';
            }

            return null;
          },
          onSaved: (String value) {
            _emailLogin = value;
            usuarioLogin.user = value;
          },
        ));
  }

  //Widget que crea el campo para ingresar el Password
  Widget _crearPassword() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: TextFormField(
          obscureText: true,
          decoration: InputDecoration(
            icon: Icon(Icons.lock_outline, color: Colors.blueGrey),
            hintText: 'Ejemplo88!',
            labelText: 'Contraseña',
          ),
          maxLength: 20,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Se requiere ingresar la Contraseña';
            }

            return null;
          },
          onSaved: (String value) {
            var bytes1 = utf8.encode(value);
            digest1 = sha256.convert(bytes1);
            _passwordLogin = digest1.toString();
          },
        ));
  }

  //Widget que crea el boton para ingresar al HomePage
  Widget _crearBotonLogin(LoginBloc bloc) {
    return FutureBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
            child: Container(
              height: 45,
              width: 240,
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Ingresar'),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 0.0,
            color: Color.fromRGBO(0, 73, 141, 1.0),
            textColor: Colors.white,
            onPressed: () {
              if (validateAndSave()) {
                setState(() {
                  res.cargarCalificacion(_listCalificada);
                  res.cargarRestaurantes(
                      _listRest, _mejores10, _suggestions, _listCalificada);

                  isApiCallProcess = true;
                });
                UserProvider userProv = new UserProvider();

                userProv
                    .validarUsuario(_passwordLogin, validation, usuarioLogin)
                    .then((value) => {
                          setState(() {
                            isApiCallProcess = false;
                          }),
                          if (usuarioLogin.pass == _passwordLogin)
                            {
                              if (usuarioLogin.state == 1)
                                {
                                  if (usuarioLogin.name.contains('#'))
                                    {createAlertDialogCambioClave(context)}
                                  else
                                    {
                                      setState(() {
                                        userProv
                                            .cargarRestaurantesFavoritosUsuario(
                                                _listFav, getUsuario);
                                      }),
                                      createAlertDialogValido(context)
                                    }
                                }
                              else
                                {createAlerDialogCuentaBloqueada(context)}
                            }
                          else
                            {createAlertDialogInvalido(context)}
                        });
              }
            });
      },
    );
  }

  //Widget que confirma que la cuenta esta bloqueada
  Future<String> createAlerDialogCuentaBloqueada(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text('Cuenta Inhabilitada'),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Cerrar Mensaje'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  }),
            ],
          );
        });
  }

  //Widget que indica cambiar la clave
  Future<String> createAlertDialogCambioClave(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text('Debe Cambiar su contraseña'),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Cerrar Mensaje'),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CambioClaveScreen()));
                  }),
            ],
          );
        });
  }

  //Widget que confirma el inicio de sesion
  Future<String> createAlertDialogValido(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text('Login Exitoso'),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Cerrar Mensaje'),
                  onPressed: () {
                    createAlertDialogUbicacion(context);
                  }),
            ],
          );
        });
  }

  //Obtiene la ubicacion actual
  Future<LatLng> getLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    latlong = new LatLng(position.latitude, position.longitude);

    return latlong;
  }

  //Widget que invalida el inicio de sesion
  Future<String> createAlertDialogInvalido(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text('Por Favor Verifique sus datos'),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Cerrar Mensaje'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  }),
            ],
          );
        });
  }

  Future<String> createAlertDialogUbicacion(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(''),
            content: Text(
                'Para ingresar a Find&Eat debe permitir obtener su ubicación'),
            actions: <Widget>[
              MaterialButton(
                  elevation: 5.0,
                  child: Text('Sí'),
                  onPressed: () {
                    permisoMapa = true;
                    print(permisoMapa);
                    getLocation().then((value) => {
                          print(latlong),
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomePage()))
                        });
                  }),
              MaterialButton(
                  elevation: 5.0,
                  child: Text('No'),
                  onPressed: () {
                    permisoMapa = false;
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  })
            ],
          );
        });
  }

  //Widget que direcciona a la pagina de registro
  Widget _crearBotonRegistro(LoginBloc bloc) {
    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
            child: Container(
              height: 45,
              width: 240,
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Registrarse'),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 0.0,
            color: Color.fromRGBO(0, 73, 141, 1.0),
            textColor: Colors.white,
            onPressed: () => Navigator.push(context,
                MaterialPageRoute(builder: (context) => RegistroScreen())));
      },
    );
  }

  //Funcion que valida los datos ingresados
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

//Widget que crea el fondo
  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final fondodegradado = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Color.fromRGBO(0, 73, 141, 1.0),
        Color.fromRGBO(90, 70, 178, 1.0)
      ])),
    );

    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          color: Color.fromRGBO(255, 255, 255, 0.05)),
    );

    return Stack(
      children: <Widget>[
        fondodegradado,
        Positioned(top: 90.0, left: 30.0, child: circulo),
        Positioned(top: -40.0, right: -30.0, child: circulo),
        Positioned(bottom: -50.0, right: -10.0, child: circulo),
        Positioned(bottom: 120.0, right: 20.0, child: circulo),
        Positioned(bottom: -50.0, left: -20.0, child: circulo),
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Icon(Icons.food_bank, color: Colors.white, size: 100.0),
              SizedBox(height: 10.0, width: double.infinity),
              Text('FIND & EAT',
                  style: TextStyle(color: Colors.white, fontSize: 25.0))
            ],
          ),
        )
      ],
    );
  }
}
