import 'package:findandeat/providers/rest_provider.dart';
import 'package:findandeat/src/models/restaurant_model.dart';
import 'package:findandeat/src/pages/google_map_screen.dart';
import 'package:findandeat/src/pages/home_page.dart';
import 'package:findandeat/src/pages/login_validation.dart';
import 'package:flutter/material.dart';
import 'package:findandeat/src/bloc/provider.dart';

//Variables Globales
double filtro;
double get getFiltro => filtro;
List<RestaurantModel> listaFiltrada = [];
List<RestaurantModel> get getListaFiltrada => listaFiltrada;

//Clase principal de tipo statefull
class FiltroScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return Cambio();
  }
}

//Clase que cambia la informacion de la clase principal
class Cambio extends State<FiltroScreen> {
  TextEditingController _filter = TextEditingController();

  bool isApiCallProcessRegister = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return LoginVal(
      child: _uiRegister(context),
      inAsyncCall: isApiCallProcessRegister,
      opacity: 0.3,
    );
  }

  @override
  //Widget que acopla la informacion
  Widget _uiRegister(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        _crearFondo(context),
        _filtroForm(context),
      ],
    ));
  }

  //Widget que muestra los campos para registrarse
  Widget _filtroForm(BuildContext context) {
    final bloc = Provider.of(context);
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
        child: Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 180.0,
            ),
          ),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 30.0),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 3.0,
                      offset: Offset(0.0, 5.0),
                      spreadRadius: 3.0)
                ]),
            child: Column(
              children: <Widget>[
                Text('Filtro de Busqueda', style: TextStyle(fontSize: 20.0)),
                SizedBox(height: 60.0),
                _crearFiltro(),
                SizedBox(height: 30.0),
                _confirmarCambio(bloc),
                SizedBox(height: 30.0),
                _cancelar(bloc),
              ],
            ),
          ),
          SizedBox(height: 100.0)
        ],
      ),
    ));
  }

  //Widget que crea el campo del Password
  Widget _crearFiltro() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: TextFormField(
          controller: _filter,
          obscureText: false,
          decoration: InputDecoration(
            icon: Icon(Icons.lock_outline, color: Colors.blueGrey),
            hintText: 'debe ser de 0 a 5',
            labelText: 'Calificación mínima',
          ),
          maxLength: 5,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Se requiere ingresar la mínima de calificación';
            } else {
              Pattern pattern = r'(^\d*\.?\d*)';
              if (num.tryParse(value) == null) {
                return 'Se requiere un valor numerico';
              }
              if (double.parse(value) < 0 || double.parse(value) > 5) {
                return 'Ingrese un valor dentro del rango';
              }
            }

            return null;
          },
          onSaved: (String value) {
            filtro = double.parse(_filter.text);
          },
        ));
  }

  //Widget que confirma el registro
  Widget _confirmarCambio(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Confirmar Datos'),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 0.0,
            color: Color.fromRGBO(0, 73, 141, 1.0),
            textColor: Colors.white,
            onPressed: () {
              if (validateAndSave()) {
                listaFiltrada.clear();
                listaFiltrada =
                    RestaurantProvider().obtenerXCalificacion(filtro);
                filtro = 0.0;
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => GoogleMapScreen()));
              }
            });
      },
    );
  }

  //Widget que crea el boton y cancela la operacion de registro
  Widget _cancelar(LoginBloc bloc) {
    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Cancelar'),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 0.0,
            color: Color.fromRGBO(0, 73, 141, 1.0),
            textColor: Colors.white,
            onPressed: () {
              filtro = 0.0;
              listaFiltrada = [];
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => HomePage()));
            });
      },
    );
  }

  //Funcion que valida los campos
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  //Widget que crea el fondo
  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final fondodegradado = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Color.fromRGBO(0, 73, 141, 1.0),
        Color.fromRGBO(90, 70, 178, 1.0)
      ])),
    );

    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          color: Color.fromRGBO(255, 255, 255, 0.05)),
    );

    return Stack(
      children: <Widget>[
        fondodegradado,
        Positioned(top: 90.0, left: 30.0, child: circulo),
        Positioned(top: -40.0, right: -30.0, child: circulo),
        Positioned(bottom: -50.0, right: -10.0, child: circulo),
        Positioned(bottom: 120.0, right: 20.0, child: circulo),
        Positioned(bottom: -50.0, left: -20.0, child: circulo),
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Icon(Icons.food_bank, color: Colors.white, size: 100.0),
              SizedBox(height: 10.0, width: double.infinity),
              Text('FIND & EAT',
                  style: TextStyle(color: Colors.white, fontSize: 25.0))
            ],
          ),
        )
      ],
    );
  }
}
