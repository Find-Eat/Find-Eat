import 'dart:convert';

//Variables Globales
UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));
String userModelToJson(UserModel data) => json.encode(data.toJson());
String userModelToJsonValidate(UserModel data) =>
    json.encode(data.toJsonValidate());

//Clase que establece el modelo de un usuario
class UserModel {
  String user;
  String name;
  String pass;
  int state;
  UserModel({
    this.pass,
    this.user,
    this.name,
    this.state,
  });
  //Recibe datos en formato JSON
  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        pass: json["pass"],
        user: json["user"],
        name: json["name"],
        state: json["state"].toInt(),
      );
  //Envía datos en formato JSON
  Map<String, dynamic> toJson() => {
        "user": user,
        "name": name,
        "pass": pass,
        "state": state,
      };
  Map<String, dynamic> toJsonValidate() => {"user": user};
}
