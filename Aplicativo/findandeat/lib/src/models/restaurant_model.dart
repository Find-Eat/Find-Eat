import 'dart:convert';

String restaurantModelToJsonValidate(RestaurantModel data) =>
    json.encode(data.toJsonValidate());

//Clase que establece el modelo de un restaurante
class RestaurantModel {
  RestaurantModel(
    this.id,
    this.title,
    this.rating,
    this.tRating,
    this.address,
    this.lat,
    this.long,
    this.tweets,
  );
  String id;
  String title;
  String rating;
  String tRating;
  String lat;
  String long;
  String address;
  List<String> tweets;
  Map<String, dynamic> toJsonValidate() => {"maps_place_id": id};
}
