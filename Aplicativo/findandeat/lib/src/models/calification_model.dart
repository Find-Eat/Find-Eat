//Clase que establece el modelo de la clasificacion del analisis de sentimientos
class CalificationModel {
  CalificationModel(
    this.id,
    this.rating,
  );
  String id;
  String rating;
}
