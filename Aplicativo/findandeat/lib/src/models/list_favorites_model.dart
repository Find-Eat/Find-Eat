import 'dart:convert';

//Variables Globales
FavoriteModel userModelFromJson(String str) =>
    FavoriteModel.fromJson(json.decode(str));
String favoriteModelToJson(FavoriteModel data) => json.encode(data.toJson());

//Clase que establece el Modelo del restaurante favorito de un usuario
class FavoriteModel {
  String user;
  String restaurant;
  FavoriteModel({
    this.user,
    this.restaurant,
  });
  factory FavoriteModel.fromJson(Map<String, dynamic> json) =>
      FavoriteModel(user: json["user"], restaurant: json["state"]);
  Map<String, dynamic> toJson() => {"user": user, "restaurant": restaurant};
}
