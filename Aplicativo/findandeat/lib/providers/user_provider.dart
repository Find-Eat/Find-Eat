import 'package:findandeat/src/models/list_favorites_model.dart';
import 'package:findandeat/src/models/user_model.dart';
import 'package:findandeat/src/pages/register.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class UserProvider {
  //Función que obtiene los usarios de la base de datos para validar el login
  Future<void> validarUsuario(String pass, bool resp, UserModel user) async {
    var modelToJson = userModelToJsonValidate(user);
    final url = Uri.https(
        'f6eykhifo7.execute-api.us-east-2.amazonaws.com', 'find/user/info');
    final respuesta = await http.post(url, body: modelToJson, headers: {
      "Accept": "application/json",
      "Access-Control_Allow_Origin": "*"
    });
    final decoded = json.decode(respuesta.body);
    if (decoded == null) return null;
    for (var u in decoded) {
      if (pass == u["pass"]) {
        user.name = u["name"];
        user.user = u["user"];
        user.pass = u["pass"];
        user.state = u["state"];
        resp = true;
      } else {
        user = null;
      }
    }
  }

  //Función que obtiene los usarios de la base de datos para validar el login
  Future<void> validarExisteUsuario(UserModel user) async {
    var modelToJson = userModelToJsonValidate(user);
    final url = Uri.https(
        'f6eykhifo7.execute-api.us-east-2.amazonaws.com', 'find/user/info');
    final respuesta = await http.post(url, body: modelToJson, headers: {
      "Accept": "application/json",
      "Access-Control_Allow_Origin": "*"
    });
    final decoded = json.decode(respuesta.body);
    if (decoded == null) return null;
    for (var u in decoded) {
      if (user.user == u["user"]) {
        user.name = u["name"];
        user.user = u["user"];
        user.pass = u["pass"];
        user.state = u["state"];
      } else {
        user = null;
      }
    }
  }

//Funcion que valida el cambio de clave del usuario
  Future<void> validarCambioClaveUsuario(
      bool resp, String correo, UserModel user) async {
    var modelToJson = userModelToJsonValidate(user);
    final url = Uri.https(
        'f6eykhifo7.execute-api.us-east-2.amazonaws.com', 'find/user/info');
    final respuesta = await http.post(url, body: modelToJson, headers: {
      "Accept": "application/json",
      "Access-Control_Allow_Origin": "*"
    });
    final decoded = json.decode(respuesta.body);
    if (decoded == null) return null;
    for (var u in decoded) {
      if (correo == u["user"]) {
        user.name = u["name"];
        user.user = u["user"];
        user.pass = u["pass"];
        user.state = u["state"];
      } else {
        user = null;
      }
    }
  }

//Funcion que crea un usuario
  Future<bool> createUser(UserModel model, int codResp) async {
    var modelToJson = userModelToJson(model);
    final url = Uri.https(
        "f6eykhifo7.execute-api.us-east-2.amazonaws.com", "find/user");
    final respuesta = await http.post(url, body: modelToJson, headers: {
      "Accept": "application/json",
      "Access-Control_Allow_Origin": "*"
    });
    setCode = codResp;
    if (respuesta.statusCode == 200) {
      return true;
    }
    return false;
  }

//Funcion que modifica un usuario
  Future<bool> modifyUser(UserModel model, int codResp) async {
    var modelToJson = userModelToJson(model);
    final url = Uri.https(
        "f6eykhifo7.execute-api.us-east-2.amazonaws.com", "find/user");
    final respuesta = await http.put(url, body: modelToJson, headers: {
      "Accept": "application/json",
      "Access-Control_Allow_Origin": "*"
    });
    setCode = codResp;
    if (respuesta.statusCode == 200) {
      return true;
    }
    return false;
  }

//Funcion que adiciona un restaurante a favoritos
  Future<bool> addFavoriteRestaurant(
      FavoriteModel model, int statudCode) async {
    var modelToJson = favoriteModelToJson(model);
    final url = Uri.https(
        "f6eykhifo7.execute-api.us-east-2.amazonaws.com", "find/favorites");
    final respuesta = await http.post(url, body: modelToJson, headers: {
      "Accept": "application/json",
      "Access-Control_Allow_Origin": "*"
    });
    if (respuesta.statusCode == 200) {
      statudCode = respuesta.statusCode;
      return true;
    }
    return false;
  }

//Funcion que carga los restaurantes favoritos del usuario
  Future<List<FavoriteModel>> cargarRestaurantesFavoritosUsuario(
      List<FavoriteModel> name, UserModel user) async {
    var modelToJson = userModelToJsonValidate(user);
    final url = Uri.https("f6eykhifo7.execute-api.us-east-2.amazonaws.com",
        "find/favorites/info");
    final respuesta = await http.post(url, body: modelToJson, headers: {
      "Accept": "application/json",
      "Access-Control_Allow_Origin": "*"
    });
    bool validacion = false;
    final decoded = json.decode(respuesta.body);
    List<FavoriteModel> favoritos = [];
    if (decoded == null) return null;
    for (var u in decoded) {
      FavoriteModel favorito = FavoriteModel();
      favorito.user = u["user"];
      favorito.restaurant = u["restaurant"];
      favoritos.add(favorito);
      for (var i = 0; i < name.length; i++) {
        if (favorito.restaurant == name[i].restaurant && validacion == false)
          validacion = true;
      }
      if (validacion == false) {
        name.add(favorito);
      }
    }
    return favoritos;
  }

//Funcion que borra un restaurante de la lista de favoritos
  Future<void> deleteFavoriteRestaurant(FavoriteModel model, int status) async {
    var modelToJson = favoriteModelToJson(model);
    final url = Uri.https(
        "f6eykhifo7.execute-api.us-east-2.amazonaws.com", "find/favorites");
    final respuesta = await http.delete(url, body: modelToJson, headers: {
      "Accept": "application/json",
      "Access-Control_Allow_Origin": "*"
    });
    if (respuesta.statusCode == 200) {
      status = respuesta.statusCode;
      return true;
    }
    return false;
  }
}
