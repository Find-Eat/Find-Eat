import 'package:findandeat/src/models/calification_model.dart';
import 'package:findandeat/src/models/restaurant_model.dart';
import 'package:findandeat/src/pages/login_page.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class RestaurantProvider {
  var restaurants = "";
  List<String> nombres = [];
  //Metodo Future que obtiene la lista de restaurantes
  Future<List<RestaurantModel>> cargarRestaurantes(
    List<RestaurantModel> name,
    List<RestaurantModel> mejores,
    List<String> nombres,
    List<CalificationModel> listaCalificados,
  ) async {
    final url = Uri.https(
        'f6eykhifo7.execute-api.us-east-2.amazonaws.com', 'find/data');
    final respuesta = await http.get(url);
    bool validacion = false;
    final Map<String, dynamic> decoded = json.decode(respuesta.body);
    List<RestaurantModel> restaurantes = [];
    if (decoded["Items"] == null) return null;
    for (var u in decoded["Items"]) {
      var rating;
      var tRating = "0.0";
      if (u["rating"] == null) {
        rating = "3.0";
      } else {
        rating = u["rating"]["S"];
      }
      RestaurantModel restaurante = RestaurantModel(
          u["place_id"]["S"],
          u["name"]["S"],
          rating,
          null,
          u["vicinity"]["S"],
          u["lat"]["S"],
          u["lng"]["S"],
          null);
      restaurantes.add(restaurante);
      for (var i = 0; i < name.length; i++) {
        if (restaurante.id == name[i].id && validacion == false)
          validacion = true;
      }
      if (validacion == false) {
        print(restaurante.title);
        name.add(restaurante);
        nombres.add(restaurante.title);
        if (double.parse(restaurante.rating) > 4.5) {
          mejores.add(restaurante);
        }
      }
    }
    cargarCalificacion(listaCalificados);
    obtenerTweetsXRestaurante(name);
    return restaurantes;
  }

  //Metodo Future que carga la calificacion del analisis de sentimientos
  Future<List<CalificationModel>> cargarCalificacion(
      List<CalificationModel> name) async {
    bool validacion = false;
    final urlSA = Uri.https(
        'f6eykhifo7.execute-api.us-east-2.amazonaws.com', 'find/sentiment');
    final respuestaSA = await http.get(urlSA);
    final Map<String, dynamic> decodedSA = json.decode(respuestaSA.body);
    if (decodedSA["Items"] == null) return null;

    for (var u in decodedSA["Items"]) {
      CalificationModel calificacion =
          CalificationModel(u["place_id"]["S"], u["rating"]["S"]);
      if (calificacion.rating == null) {
        calificacion.rating = "3.0";
      }
      for (var i = 0; i < name.length; i++) {
        if (calificacion.id == name[i].id && validacion == false)
          validacion = true;
      }
      if (validacion == false) {
        print(calificacion.rating);
        name.add(calificacion);
      }
    }
    return name;
  }

//Metodo que junta las dos calificaciones
  void juntarCalificacion(
      List<RestaurantModel> name, List<CalificationModel> calificados) {
    for (var i = 0; i < name.length; i++) {
      for (var n = 0; n < calificados.length; n++) {
        if (calificados[n].id == name[i].id) {
          name[i].tRating = calificados[n].rating;
          print("Calificacion ${name[i].title}: ${name[i].tRating}");
        } else {
          name[i].tRating = "N/A";
        }
      }
    }
  }

  //Obtiene un restaurante por su nombre
  RestaurantModel obtenerRestaurant(
      String nombre, List<RestaurantModel> restaurantes) {
    RestaurantModel restaurante;
    for (var i = 0; i < restaurantes.length; i++) {
      if (nombre == restaurantes[i].title) {
        restaurante = RestaurantModel(
            restaurantes[i].id,
            restaurantes[i].title,
            restaurantes[i].rating,
            restaurantes[i].tRating,
            restaurantes[i].address,
            restaurantes[i].lat,
            restaurantes[i].long,
            restaurantes[i].tweets);
      }
    }
    return restaurante;
  }

//Filtro de calificacion para busqueda en el mapa
  List<RestaurantModel> obtenerXCalificacion(double filtro) {
    List<RestaurantModel> restaurantesFiltrados = [];
    for (var i = 0; i < listRest.length; i++) {
      if (double.parse(listRest[i].rating) >= filtro) {
        restaurantesFiltrados.add(listRest[i]);
      }
    }
    return restaurantesFiltrados;
  }

//Obtiene los tweets asociados a los restaurantes
  Future<List<String>> obtenerTweetsXRestaurante(
      List<RestaurantModel> restaurantes) async {
    final urlTweets = Uri.https(
        'f6eykhifo7.execute-api.us-east-2.amazonaws.com', 'find/tweet');
    List<String> tweets = [];
    for (var i = 0; i < restaurantes.length; i++) {
      var restVal = restaurantes[i];
      var modelToJson = restaurantModelToJsonValidate(restVal);
      final respuestaTweet = await http.post(urlTweets,
          body: modelToJson,
          headers: {
            "Accept": "application/json",
            "Access-Control_Allow_Origin": "*"
          });
      final Map<String, dynamic> decodedTweet =
          json.decode(respuestaTweet.body);

      if (decodedTweet["Items"] != null) {
        for (var x in decodedTweet["Items"]) {
          if (restaurantes[i].id == x["maps_place_id"]) {
            tweets.add(x["text"]);
            print("${restaurantes[i].title}  tweets asociado");
          } else {
            print("${restaurantes[i].title}  no hay coincidencia");
          }
        }
        restaurantes[i].tweets = tweets;
      } else {
        tweets = [];
      }
    }

    return tweets;
  }

//Carga los nombres de los restaurantes
  Future<List<RestaurantModel>> cargarNombreRestaurantes(
      List<String> name) async {
    final url = Uri.https(
        'f6eykhifo7.execute-api.us-east-2.amazonaws.com', 'find/data');
    final respuesta = await http.get(url);
    bool validacion = false;
    final Map<String, dynamic> decoded = json.decode(respuesta.body);
    List<String> tweets = [];
    List<RestaurantModel> restaurantes = [];
    if (decoded["Items"] == null) return null;
    for (var u in decoded["Items"]) {
      var rating;
      var tRating2 = "0.0";
      if (u["rating"] == null) {
        rating = "3.0";
      } else {
        rating = u["rating"]["S"];
      }
      RestaurantModel restaurante = RestaurantModel(
          u["place_id"]["S"],
          u["name"]["S"],
          rating,
          tRating2,
          u["vicinity"]["S"],
          u["lat"]["S"],
          u["lng"]["S"],
          tweets);
      if (restaurante.rating == null) {
        restaurante.rating = "3.0";
      }
      restaurantes.add(restaurante);

      for (var i = 0; i < name.length; i++) {
        if (restaurante.title == name[i] && validacion == false)
          validacion = true;
      }
      if (validacion == false) {
        name.add(restaurante.title);
      }
    }
  }
}
